What is it ?
============

**Keysas** is a modern decontamination station prototype, fast, multithreaded which aims to be secure.

**Keysas** provides an easy solution to verify your documents. 
It can be used as a simple “decontamination station” or act as a gateway to transfert untrusted files to higher security level networks. 
Untrusted files are scanned by an antivirus and you can manage and create your own Yara rules and add them easily to Keysas in order to inspect your files with more efficiency.
You can also create a whitelist of file types you want to transfert based on their **magic number** to filter more accurately. 


This software has been intentionally made to be as secure as possible using Rust programming langage, Secure Computing mode 2, strong sandboxing through systemd and many other mechanisms.
However if you encounter any security issue, do not hesitate to contact us via `Gitlab <https://gitlab.com/r3dlight/keysas/>`_.

**Keysas** can be installed on every GNU / Linux distributions based on systemd with apparmor enabled as we do not support SELinux yet.

This software is written in `Rust <https://www.rust-lang.org/>`_, under `GPL-3.0 license <https://gitlab.com/r3dlight/keysas/-/blob/master/LICENSE>`_.

Learn more and contribute on `Gitlab <https://gitlab.com/r3dlight/keysas/>`_.

