.. Keysas documentation master file, created by
   sphinx-quickstart on Wed Dec 30 08:13:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Network gateway (keysas-core)
-----------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   administration
   usage
   download
