********
Download
********

Lastest release
==================

Keysas v1.2.0 (x86_64 only)
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `keysas-v1.2.0.zip <https://keysas.fr/download/keysas-v1.2.0.zip>`_
 * `keysas-v1.2.0.zip.sig <https://keysas.fr/download/keysas-v1.2.0.zip.sig>`_
 * `keysas-v1.2.0.zip.sha256 <https://keysas.fr/download/keysas-v1.2.0.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Use of scoped threadpools;
 * Only syscall read, write, close, sigalstack, munmap and exit_group are used when writing to name pipes;
 * Bump dependencies;
 * Fix race condition when applying seccomp filters into writting threads.



Previous releases
==================


