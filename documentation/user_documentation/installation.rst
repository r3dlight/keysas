************
Installation
************

You can get **Keysas** either from source or from a pre-compiled binary.
The following installation steps will therefore be annotated to take the source base
installation in consideration.

Software dependencies
---------------------

.. code-block:: shell-session

  $ sudo echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" > /etc/apt/sources.list.d/backports.list
  $ sudo apt update
  $ sudo apt -qy -t bullseye-backports install libyara9
  $ sudo apt install \
               acl clamav-daemon clamav-freshclam libyara9 make \
               pkg-config bash ssh rsync apparmor

.. admonition:: For a source based installation
 :class: note

 .. code-block:: bash

  $ sudo echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" > /etc/apt/sources.list.d/backports.list
  $ sudo apt update
  $ sudo apt -qy -t bullseye-backports install libyara-dev libyara9
  $ sudo apt install \
              libseccomp2 libseccomp-dev \
              lsb-release pkg-config bash software-properties-common \
              build-essential libudev-dev

  # Install rustup
  $ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

  # Install the LLVM toolchain (x86_64 only)
  $ bash -c "$(wget -O - https://apt.llvm.org/llvm.sh)"

Getting **Keysas** (Network gateway)
------------------------------------

Thanks to the Gitlab CI a pre-compiled **Keysas** binary is at your
disposal, you can choose and download a specific version of **Keysas**
using the :ref:`download section <download>`.

Download the following files of lastest stable version.
 * keysas-vx.y.z.zip
 * keysas-vx.y.z.zip.sha256
 * keysas-vx.y.z.zip.sig

First, verify the sha256sum and compare it to the keysas-vx.y.z.zip.sha256
file, and import our public gpg key:

.. code-block:: shell-session

 $ diff <(sha256sum keysas-vx.y.z.zip) keysas-vx.y.z.zip.sha256 
 $ wget https://keysas.fr/download/public.gpg
 $ gpg2 --import public.gpg
 $ gpg2 --verify keysas-vx.y.z.zip.sig keysas-vx.y.z.zip
 $ unzip keysas-vx.y.z.zip -d keysas


.. warning::
 Ensure that /usr/sbin is present in your $PATH. If not, add it:

 .. code-block:: shell-session

  $ export PATH=$PATH:/usr/sbin

.. admonition:: For a source based installation
 :class: note

 Clone the gitlab repository and compile **Keysas**

 .. code-block:: shell-session

  $ git clone --depth=1 https://gitlab.com/r3dlight/keysas.git
  $ cd keysas
  $ make build

Clamav configuration
--------------------

**Keysas** uses Clamav as a virus scanner for now but additionnal scanners
could be added in future. You should update your Clamav signature database on regular bases.
This operation is handled by the **clamav-freshclam** daemon, you have to enable it.

Make sure that your **clamav-daemon** and **clamav-freshclam** services are up and running

.. code-block:: shell-session

 $ systemctl status clamav-daemon clamav-freshclam

.. admonition:: Edit the Clamav configuration
 :class: note

 Enable TCP listening on the `loopback` interface using `port 3310`

 .. code-block:: bash
 
  #/etc/clamav/clamd.conf
  TCPSocket 3310
  TCPAddr 127.0.0.1

We now need to allow the Clamav daemon to be able to read the /var/local/in
directory with Apparmor.

.. admonition:: Clamav apparmor profile tweak
  :class: note

  Add the the following **Clamav** apparmor rules to authorise **Clamd** scanning the
  entry point:

  .. code-block:: bash

    #/etc/apparmor.d/local/usr.sbin.clamd
    /var/local/in/ r,
    /var/local/in/* kr,
    /var/local/in/** kr,

  If not, add it manually and reload your apparmor configuration
  
  .. code-block:: shell-session
  
   $ sudo apparmor_parser -r /etc/apparmor.d/usr.sbin.clamd

You can now manually run a signature database update and restart the **Clamav**
daemon to take the new configurations in account.

.. code-block:: shell-session

 $ sudo systemctl start clamav-freshclam
 $ sudo systemctl restart clamav-daemon

System wide installation
------------------------

You can now install **Keysas-core** on your system.

.. code-block:: shell-session

 $ cd keysas
 $ sudo make install-core
 $ sudo make install-yararules

To install the USB version of **Keysas** (decontamination station):

.. code-block:: shell-session

 $ cd keysas
 $ sudo make install
 $ sudo make install-yararules

At the end of the installation, you should see something like this:

.. image:: /img/install_completed.png 

.. admonition:: Installation details
  :class: note

     - Every binaries (ELF) are installed under **/usr/bin/** ;
     - Systemd units are installed under **/etc/systemd/system/** ;
     - Apparmor profiles are installed under **/etc/apparmor.d/** ;
     - Configuration files are installed under **/etc/keysas/** ;
     - Log directories are created under **/var/log/** ;
     - Yara rules are installed under **/usr/share/keysas/rules**.



You can now check that every services are up and running (core mode):

.. code-block:: shell-session

 $ systemctl status keysas keysas-in keysas-transit keysas-out

If you want to check the full installation (USB mode):

.. code-block:: shell-session

 $ systemctl status keysas keysas-in keysas-transit keysas-out keysas-udev keysas-backend