// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-out".
 *
 * (C) Copyright 2019-2023 Stephane Neveu
 *
 * This file contains the main function.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

#[macro_use]
extern crate log;

use clap::{arg, crate_version, Command};
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use landlock::{
    path_beneath_rules, Access, AccessFs, Ruleset, RulesetAttr, RulesetCreatedAttr, RulesetError,
    RulesetStatus, ABI,
};
use std::path::Path;
use std::process;
use std::thread as main_thread;
use std::time::Duration;

mod errors;
mod sandbox;
use crate::errors::*;
mod keysas;

fn landlock_sandbox() -> Result<(), RulesetError> {
    let abi = ABI::V1;
    let status = Ruleset::new()
        .handle_access(AccessFs::from_all(abi))?
        .create()?
        // Read-only access.
        .add_rules(path_beneath_rules(
            &["/etc/keysas", "/run/diode-out"],
            AccessFs::from_read(abi),
        ))?
        // Read-write access.
        .add_rules(path_beneath_rules(
            &["/var/local/out", "/var/log/keysas-out"],
            AccessFs::from_all(abi),
        ))?
        .restrict_self()?;
    match status.ruleset {
        // The FullyEnforced case must be tested.
        RulesetStatus::FullyEnforced => println!("Landlock: Fully sandboxed."),
        RulesetStatus::PartiallyEnforced => println!("Landlock: Partially sandboxed."),
        // Users should be warned that they are not protected.
        RulesetStatus::NotEnforced => {
            println!("Landlock: Not sandboxed! Please update your kernel.")
        }
    }
    Ok(())
}

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    // Start clap CLI definition
    let matches = Command::new("Keysas-Out")
        .version(crate_version!())
        .author("Stephane N")
        .about("Keysas-Out, output window.")
        .arg(
            arg!( -g --keysasout <PATH> "Sets the keysasout path for transfering files")
                .default_value("/var/local/out/"),
        )
        .arg(
            arg!( -d --diodeout <PATH> "Sets the diode-out path for transfering files")
                .default_value("/run/diode-out/"),
        )
        .arg(
            arg!( -l --log <PATH> "Sets a custom log path").default_value("/var/log/lekeysas-out/"),
        )
        .arg(
            arg!( -s --rotate_over_size <SIZE> "Rotate logs over specified size (bytes)")
                .default_value("3000000")
                .value_parser(clap::value_parser!(u64)),
        )
        .arg(
            arg!( -n --nb_of_log_files <NUMBER> "Number of log files to keep")
                .default_value("3")
                .value_parser(clap::value_parser!(usize)),
        )
        .get_matches();

    // Get values or use default ones.
    // Unwrap() cannot panic as all values are defaulted. See Clap v4.x.
    let keysasout = matches.get_one::<String>("keysasout").unwrap();
    let diode = matches.get_one::<String>("diodeout").unwrap();
    let log_path = matches.get_one::<String>("log").unwrap();
    let rotate_over_size = matches.get_one::<u64>("rotate_over_size").unwrap();
    let nb_of_log_files = matches.get_one::<usize>("nb_of_log_files").unwrap();

    //let rotate_over_size = rotate_over_size
    //    .parse::<u64>()
    //    .context("Cannot convert rotate_over_size value string into u64 integer !")?;
    //let nb_of_log_files = nb_of_log_files
    //    .parse::<usize>()
    //    .context("Cannot convert nb_of_log_files value string into usize integer !")?;

    // Check that log_path is valid.
    if !Path::new(log_path).exists()
        || !Path::new(log_path).is_dir()
        || Path::new(log_path).metadata()?.permissions().readonly()
    {
        panic!("Log path {} is not valid ! Please check if the directory exists and check the permissions.", log_path)
    }

    // Logger init and parameters.
    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Warn)
        .rotate(
            Criterion::Size(*rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(*nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // Setting up Lanlock sandbox.
    #[cfg(target_os = "linux")]
    match landlock_sandbox() {
        Ok(_) => (),
        Err(_e) => {
            error!("Failed to start landlock sandbox");
            process::exit(1);
        }
    }
    // Setting up allowed syscalls.
    #[cfg(target_os = "linux")]
    match sandbox::init().context("Cannot apply first seccomp mode 2 syscall filter") {
        Ok(_) => (),
        Err(_e) => {
            error!("Cannot apply first seccomp mode 2 syscall filter");
            process::exit(1);
        }
    }

    if !Path::new(diode).exists()
        || !Path::new(diode).is_dir()
        || Path::new(diode).metadata()?.permissions().readonly()
    {
        error!(
            "Path to diode-out {} not valid ! This should normaly be created by systemd.",
            diode
        );
        process::exit(1);
    }

    if !Path::new(keysasout).exists()
        || !Path::new(keysasout).is_dir()
        || Path::new(keysasout).metadata()?.permissions().readonly()
    {
        error!("Path to keysasout {} is not valid ! Please check if the directory exists and check the permissions.", keysasout);
        process::exit(1);
    }

    // Logger is set, so I can use it.
    info!("{}", "Keysas-Out is starting");
    info!("Watching directory for files...");
    info!("Outgoing sas : {}", keysasout);
    info!("Out diode path : {}", diode);
    info!("Log Path : {}", log_path);
    info!("Rotate logs over size : {:?}", rotate_over_size);
    info!("Number of logs to keep : {:?}", nb_of_log_files);

    // Starting the main loop.
    loop {
        //Sleep a bit to save CPU
        main_thread::sleep(Duration::from_millis(120));
        //Read data from every mkfifo
        keysas::read_files(keysasout, diode)?;
    }
}
