// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-out".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the tests.
 */

#[cfg(test)]
use super::*;

#[test]
fn test_write_and_hash_file() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path_diodeout = dir.path().join("diodeout");
    let path_keysasout = dir.path().join("keysasout");

    fs::create_dir_all(&path_diodeout).unwrap();
    assert_eq!(true, Path::new(&path_diodeout).exists());
    fs::create_dir_all(&path_keysasout).unwrap();
    assert_eq!(true, Path::new(&path_keysasout).exists());

    let file = &path_diodeout.join("file.txt");
    let _output = File::create(file).unwrap();
    assert_eq!(true, Path::new(file).exists());
    let keysasout = format!("{}{}", &path_keysasout.to_str().unwrap(), "/");
    let diodeout = format!("{}{}", &path_diodeout.to_str().unwrap(), "/");
    write_and_hash_file("file.txt", &diodeout, &keysasout).unwrap();
    let newfile = &path_keysasout.join("file.txt");
    assert_eq!(true, Path::new(newfile).exists());
    drop(dir);
}

#[test]
fn test_read_files() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("diodeout");
    let _output = fs::create_dir_all(&path).unwrap();
    let file = path.join("file.txt");
    let file2 = path.join("file2.txt");
    let file3 = path.join("file3.txt");
    let _output = File::create(&file).unwrap();
    let _output = File::create(&file2).unwrap();
    let _output = File::create(&file3).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&file2).exists());

    let path_keysasout = dir.path().join("keysasout");
    let _output = fs::create_dir_all(&path_keysasout).unwrap();

    let diode = format!("{}{}", &path.to_str().unwrap(), "/");
    let path_keysasout_string = format!("{}{}", &path_keysasout.to_str().unwrap(), "/");

    read_files(&path_keysasout_string, &diode).unwrap();
    let file = path_keysasout.join("file.txt");
    let file2 = path_keysasout.join("file2.txt");
    let file3 = path_keysasout.join("file3.txt");
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&file3).exists());
    drop(dir);
}
