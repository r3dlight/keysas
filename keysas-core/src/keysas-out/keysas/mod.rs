// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-out".
 *
 * (C) Copyright 2019-2023 Stephane Neveu
 *
 * This file contains various funtions
 * for building the keysas-in binary.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

use crate::errors::*;
use scoped_thread_pool::Pool;
use sha2::{Digest, Sha256};
use std::fs::File;
use std::io::prelude::*;
use std::io::BufRead;
use std::path::Path;
use std::{fs, io::BufReader};
use syscallz::{Context as cont, Syscall};
mod test_mod;
use std::sync::{Arc, Mutex, Once};

//Read from named pipe and write them to filesystem
pub fn read_files(keysasout: &str, diode: &str) -> Result<()> {
    let paths: std::fs::ReadDir = fs::read_dir(diode)?;
    let names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();

    //Create a worker pool
    let pool = Pool::new(num_cpus::get());

    // Create a per thread static init counter
    thread_local! (static INIT: Once = Once::new());

    // Create a mutex for the seccomp configuration
    let seccomp_lock = Arc::new(Mutex::new(true));

    pool.scoped(|scope| {
        for name in names {
            // Create a copy of the lock for each thread
            let lock = Arc::clone(&seccomp_lock);
            scope.execute(move || {
                // Test if already initialized
                INIT.with(|init| {
                    init.call_once(|| {
                        // Get the lock
                        let data = lock.lock().unwrap();
                        // Apply the seccomp configuration
                        #[cfg(target_os = "linux")]
                        init_seccomp_thread_write()
                            .expect("Cannot apply seccomp mode 2 syscall filter into thread");
                        // Release the lock
                        drop(data);
                    });
                });

                match write_and_hash_file(name.as_str(), diode, keysasout) {
                    Ok(()) => debug!("write_file ok for file: {}.", name.as_str()),
                    Err(e) => error!("write_file: Error writing file {}: {:?}", name.as_str(), e),
                }
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// write_and_hash_file(path_to_file: &str, path_to_write: &str)
/// This function read the content of a file at `path_to_file`,
/// copy it in a new file at `path_to_write` and log its digest.
/// The function returns an error if :
///     - the input file cannot be open
///     - the input file cannot be read
///     - the output file cannot be writen
fn write_and_hash_file(name: &str, in_dir: &str, out_dir: &str) -> Result<()> {
    let in_path = format!("{}{}", in_dir, name);
    let out_name = format!("{}{}", out_dir, name);
    if Path::new(&in_path).exists() {
        let in_file = File::options()
            .read(true)
            .write(false)
            .open(in_path)
            .context("Cannot open data to write into the filesystem.")?;
        let mut out_file = File::create(out_name).context("Cannot create output file.")?;
        let mut sha256: Sha256 = Sha256::new();
        let mut reader = BufReader::with_capacity(1048576, in_file);

        loop {
            let length = {
                let buffer = reader.fill_buf()?;
                out_file.write_all(buffer)?;
                sha256.update(buffer);
                buffer.len()
            };
            if length == 0 {
                break;
            }
            reader.consume(length);
        }
        let hash = sha256.finalize();
        info!("File {} sha256 hash is: {:x}", name, hash);
    } else {
        warn!("write_file: path to diode doesn't exists anymore !");
    }
    Ok(())
}

#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init_seccomp_thread_write() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::write)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::exit)?;
    ctx.allow_syscall(Syscall::openat)?;
    ctx.allow_syscall(Syscall::statx)?;
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::madvise)?;
    ctx.allow_syscall(Syscall::fcntl)?;
    ctx.allow_syscall(Syscall::futex)?;
    ctx.allow_syscall(Syscall::fstat)?;
    ctx.allow_syscall(Syscall::getdents64)?;
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::mprotect)?;
    ctx.allow_syscall(Syscall::mmap)?;
    ctx.load()?;
    Ok(())
}
