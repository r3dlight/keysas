// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-transit".
 *
 * (C) Copyright 2019-2023 Stephane Neveu, Luc Bonnafoux
 *
 * This file contains the main function.
 */

#![forbid(unsafe_code, private_in_public)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

#[macro_use]
extern crate log;

use clap::{arg, crate_version, Command};
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use landlock::{
    path_beneath_rules, Access, AccessFs, Ruleset, RulesetAttr, RulesetCreatedAttr, RulesetError,
    RulesetStatus, ABI,
};
use std::path::Path;
use std::process;
use std::thread as main_thread;
use std::time::Duration;

mod errors;
mod sandbox;
use crate::errors::*;
mod keysas;

fn landlock_sandbox() -> Result<(), RulesetError> {
    let abi = ABI::V1;
    let status = Ruleset::new()
        .handle_access(AccessFs::from_all(abi))?
        .create()?
        // Read-only access.
        .add_rules(path_beneath_rules(
            &["/etc/keysas", "/run/diode-in", "/usr/share/keysas"],
            AccessFs::from_read(abi),
        ))?
        // Read-write access.
        .add_rules(path_beneath_rules(
            &[
                "/run/diode-out",
                "/var/local/transit",
                "/var/log/keysas-transit",
            ],
            AccessFs::from_all(abi),
        ))?
        .restrict_self()?;
    match status.ruleset {
        // The FullyEnforced case must be tested.
        RulesetStatus::FullyEnforced => println!("Landlock: Fully sandboxed."),
        RulesetStatus::PartiallyEnforced => println!("Landlock: Partially sandboxed."),
        // Users should be warned that they are not protected.
        RulesetStatus::NotEnforced => {
            println!("Landlock: Not sandboxed! Please update your kernel.")
        }
    }
    Ok(())
}

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    // Start clap CLI definition

    let matches = Command::new("Keysas-Transit")
        .version(crate_version!())
        .author("r3dlight")
        .about("Keysas-Transit, transit window.")
        .arg(arg!( -g --sastransit <PATH> "Sets le-keysas's path for transiting files").default_value("/var/local/transit/")
        )
        .arg(arg!( -i --diodein <PATH> "Sets the diode-in path for transiting files").default_value("/run/diode-in/")
        )
        .arg(arg!( -o --diodeout <PATH> "Sets the diode-out path for transfering files").default_value("/run/diode-out/")
        )
        .arg(arg!( -l --log <PATH> "Sets the path to logs").default_value("/var/log/keysas-transit/")
        )
        .arg(arg!( -r --rules_path <PATH> "Sets a custom path for Yara rules").default_value("/usr/share/keysas/rules/index.yar")
        )
        .arg(arg!( -t --yara_timeout <SECONDS> "Sets a custom timeout for libyara scans").default_value("900").value_parser(clap::value_parser!(u16))
        )
        .arg(arg!( -m --yara_maxfilesize <SIZE> "Max file size for libyara to scan").default_value("50000000").value_parser(clap::value_parser!(u64))
        )
        .arg(arg!( -c --yara_clean <BOOL> "Remove the file if at least one rule matchs").default_value("false").value_parser(clap::value_parser!(String))
        )
        .arg(arg!( -s --rotate_over_size <SIZE> "Rotate logs over specified size (bytes)").default_value("3000000").value_parser(clap::value_parser!(u64))
        )
        .arg(arg!( -n --nb_of_log_files <NUMBER> "Number of log files to keep").default_value("3").value_parser(clap::value_parser!(usize))
        )
        .arg(arg!( -a --allowed_formats <LIST> "Whitelist (comma separated) of allowed file formats").default_value("jpg,png,gif,bmp,mp4,m4v,avi,wmv,mpg,flv,mp3,wav,ogg,epub,mobi,doc,docx,xls,xlsx,ppt,pptx")
        )
        .get_matches();

    // Get values or use default ones.
    // Unwrap() cannot panic as all values are defaulted. See Clap v4.x.
    let keysas_transit = matches.get_one::<String>("sastransit").unwrap();
    let diode_in = matches.get_one::<String>("diodein").unwrap();
    let diode_out = matches.get_one::<String>("diodeout").unwrap();
    let log_path = matches.get_one::<String>("log").unwrap();
    let rules_path = matches.get_one::<String>("rules_path").unwrap();

    let yara_timeout = matches.get_one::<u16>("yara_timeout").unwrap();

    //let yara_timeout = yara_timeout
    //    .parse::<u16>()
    //    .context("Cannot convert Yara timeout value string into u16 integer !")?;

    let yara_maxfilesize = matches.get_one::<u64>("yara_maxfilesize").unwrap();
    //let yara_maxfilesize = yara_maxfilesize
    //    .parse::<u64>()
    //    .context("Cannot convert Yara max file size value string into u64 integer !")?;
    let rotate_over_size = matches.get_one::<u64>("rotate_over_size").unwrap();
    let nb_of_log_files = matches.get_one::<usize>("nb_of_log_files").unwrap();
    let yara_clean = matches.get_one::<String>("yara_clean").unwrap();
    let yara_clean = yara_clean
        .parse::<bool>()
        .context("Cannot convert Yara_clean value string into boolean !")?;

    //let rotate_over_size = rotate_over_size
    //    .parse::<u64>()
    //    .context("Cannot convert rotate_over_size value string into u64 integer !")?;
    //let nb_of_log_files = nb_of_log_files
    //    .parse::<usize>()
    //    .context("Cannot convert nb_of_log_files value string into usize integer !")?;
    let magic_list = matches.get_one::<String>("allowed_formats").unwrap();
    let magic_list = magic_list.split(',');
    let magic_list: Vec<&str> = magic_list.collect();

    // Check that log_path is valid.
    if !Path::new(log_path).exists()
        || !Path::new(log_path).is_dir()
        || Path::new(log_path).metadata()?.permissions().readonly()
    {
        panic!("Log path {} is not valid ! Please check if the directory exists and check the permissions.", log_path)
    }

    // Logger init and parameters.
    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Warn)
        .rotate(
            Criterion::Size(*rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(*nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // Setting up Lanlock sandbox.
    #[cfg(target_os = "linux")]
    match landlock_sandbox() {
        Ok(_) => (),
        Err(_e) => {
            error!("Failed to start landlock sandbox");
            process::exit(1);
        }
    }
    // Setting up allowed syscalls.
    #[cfg(target_os = "linux")]
    match sandbox::init().context("Cannot apply first seccomp mode 2 syscall filter") {
        Ok(_) => (),
        Err(_e) => {
            error!("Cannot apply first seccomp mode 2 syscall filter");
            process::exit(1);
        }
    }

    // Check that diode_in, diode_out and keysas_transit exist before going futher.
    if !Path::new(diode_out).exists()
        || !Path::new(diode_out).is_dir()
        || Path::new(diode_out).metadata()?.permissions().readonly()
    {
        error!(
            "Path to diode-out {} not valid ! This should normaly be created by systemd.",
            diode_out
        );
        process::exit(1);
    }

    if !Path::new(diode_in).exists()
        || !Path::new(diode_in).is_dir()
        || Path::new(diode_in).metadata()?.permissions().readonly()
    {
        error!(
            "Path to diode-in {} not valid ! This should normaly be created by systemd.",
            diode_in
        );
        process::exit(1);
    }

    if !Path::new(keysas_transit).exists()
        || !Path::new(keysas_transit).is_dir()
        || Path::new(keysas_transit)
            .metadata()?
            .permissions()
            .readonly()
    {
        error!("Path to transit keysas {} is not valid !", keysas_transit);
        process::exit(1);
    }

    // Check that path to yara rules is valid.
    if !Path::new(rules_path).exists()
        || !Path::new(rules_path).is_file()
        || Path::new(rules_path).metadata()?.permissions().readonly()
    {
        error!("Path to Yara index file ({}) is not valid !", rules_path);
        process::exit(1);
    }

    let allowed_magics: Vec<&str> = vec![
        "jpg", "png", "gif", "webp", "cr2", "tif", "bmp", "heif", "avif", "jxr", "psd", "ico",
        "mp4", "m4v", "mkv", "webm", "mov", "avi", "wmv", "mpg", "flv", "mid", "mp3", "m4a", "ogg",
        "flac", "wav", "amr", "aac", "epub", "zip", "tar", "rar", "gz", "bz2", "7z", "xz", "pdf",
        "swf", "rtf", "eot", "ps", "sqlite", "nes", "crx", "cab", "deb", "ar", "Z", "lz", "rpm",
        "dcm", "zst", "msi", "mobi", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "odt", "ods",
        "odp", "woff", "woff2", "ttf", "otf", "wasm", "exe", "dll", "elf", "bc", "mach", "class",
        "dex", "dey", "der", "obj", "sh", "xml", "html", "aiff", "dsf",
    ];
    for selected_type in magic_list.iter() {
        if !allowed_magics.contains(selected_type) {
            error!("Unknown value in allowed_types: {}", selected_type);
            process::exit(1);
        }
    }

    // Logger is set, so I can use it.
    info!("{}", "Keysas-Transit is starting");
    info!("Transit sas : {}", keysas_transit);
    info!("In diode path : {}", diode_in);
    info!("Out diode path : {}", diode_out);
    info!("Rotate logs over size : {:?}", rotate_over_size);
    info!("Number of logs to keep : {:?}", nb_of_log_files);
    info!("Yara max file size: {}", yara_maxfilesize);
    info!("Yara timeout: {}", yara_timeout);
    info!("Remove file if Yara matched: {}", yara_clean);
    info!("Allowed file formats: {:?}", magic_list);
    println!("Allowed file formats: {:?}", magic_list);

    // Housekeeping while starting up: removing everything
    match keysas::clean_sas_dir(keysas_transit) {
        Ok(_o) => (),
        Err(_e) => {
            error!("Cannot clean files in keysas transit path.");
            process::exit(1);
        }
    }

    // Starting the main loop.
    loop {
        match keysas::read_files(keysas_transit, diode_in) {
            Ok(_o) => (),
            Err(_e) => {
                error!("Error while reading incoming files");
                process::exit(1);
            }
        }
        main_thread::sleep(Duration::from_millis(120));
        match keysas::analyse_transit_files(
            keysas_transit,
            rules_path,
            *yara_timeout,
            *yara_maxfilesize,
            yara_clean,
            magic_list.clone(),
        ) {
            Ok(_o) => (),
            Err(_e) => {
                error!("Error while analysing files");
                process::exit(1);
            }
        }
        match keysas::send_transit_files(keysas_transit, diode_out) {
            Ok(_o) => (),
            Err(_e) => {
                error!("Error while transmiting file to out");
                process::exit(1);
            }
        }
    }
}
