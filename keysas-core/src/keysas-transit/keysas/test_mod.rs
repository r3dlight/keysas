// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-transit".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the tests.
 */

#[cfg(test)]
use super::*;

#[test]
fn test_remove() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    let _output = File::create(&file).unwrap();
    remove(file.to_str().unwrap());
    assert_eq!(false, Path::new(&file).exists());
    let dir = tempdir().unwrap();
    let path = dir.path().join("directory");
    let _output = fs::create_dir_all(&path).unwrap();
    assert_eq!(true, Path::new(&path).exists());
    remove(path.to_str().unwrap());
    assert_eq!(false, Path::new(&path).exists());
    drop(dir);
}

#[test]
fn test_list_files() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("transit");
    let _output = fs::create_dir_all(&path).unwrap();
    assert_eq!(true, Path::new(&path).exists());
    let file = path.join("file.txt");
    let _output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    let files = list_files(path.to_str().unwrap());
    assert_eq!(files.unwrap(), ["file.txt"]);
    drop(dir);
}

#[test]
fn test_mkfifo() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("test_fifo");
    mkfifo(&path.to_str().unwrap());
    assert_eq!(true, Path::new(&path).exists());
    remove(&path.to_str().unwrap());
    assert_ne!(true, Path::new(&path).exists());
    drop(dir)
}
#[test]
fn test_analyse_transit_files() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let keysastransit = dir.path().join("fake_keysastransit");
    let diodeout = dir.path().join("fake_diodetransit");
    fs::create_dir_all(&keysastransit).unwrap();
    fs::create_dir_all(&diodeout).unwrap();
    assert_eq!(true, Path::new(&keysastransit).exists());
    assert_eq!(true, Path::new(&keysastransit).exists());
    let file_sha256 = keysastransit.join("file.sha256");
    let _output = File::create(&file_sha256).unwrap();
    let file_yara = keysastransit.join("file.yara");
    let _output = File::create(&file_yara).unwrap();
    let file_av = keysastransit.join("file.antivirus");
    let _output = File::create(&file_av).unwrap();
    let file_toobig = keysastransit.join("file.toobig");
    let _output = File::create(&file_toobig).unwrap();
    let file_failed = keysastransit.join("file.failed");
    let _output = File::create(&file_failed).unwrap();
    let file_7z = keysastransit.join("file.7z");
    let _output = File::create(&file_7z).unwrap();
    let file = keysastransit.join("file");
    let _output = File::create(&file).unwrap();
    let directory = keysastransit.join("directory");
    let _output = fs::create_dir_all(&directory).unwrap();

    let filerule = dir.path().join("index.yar");

    let rule = "rule contains_rust {
                    strings:
                        $rust = \"rust\" nocase
                    condition:
                        $rust
                }";
    fs::write(&filerule, rule).expect("Unable to write fake index rule");
    let filescan = keysastransit.join("file.txt");
    fs::write(&filescan, "hello rust !").expect("Unable to write yara_scan.txt for testing");
    assert_eq!(true, Path::new(&filescan).exists());

    let keysastransit = format!("{}{}", keysastransit.to_str().unwrap(), "/");
    let allowed = vec![""];
    analyse_transit_files(
        &keysastransit,
        filerule.to_str().unwrap(),
        800,
        100000,
        true,
        allowed,
    )
    .unwrap();
    //Mime type and extension will not be 7z nor zip here, so it should not pass !
    assert_eq!(false, file_7z.exists());
    assert_eq!(false, file.exists());
    //The following test proves that if yara matched the rule, the file is deleted
    assert_ne!(true, filescan.exists());
    assert_eq!(true, file_av.exists());
    assert_eq!(true, file_toobig.exists());
    assert_eq!(true, file_failed.exists());
    assert_eq!(true, file_yara.exists());
    assert_eq!(true, file_sha256.exists());
    assert_ne!(true, directory.exists());
    drop(dir);
}
#[test]
fn test_write_and_hash_file() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let keysastransit = dir.path().join("fake_keysastransit");
    fs::create_dir_all(&keysastransit).unwrap();
    assert_eq!(true, keysastransit.exists());
    let diodein = dir.path().join("fake_diodein");
    fs::create_dir_all(&diodein).unwrap();
    assert_eq!(true, diodein.exists());
    let file = diodein.join("file.txt");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    let outfile = keysastransit.join("file.txt");
    let keysastransit = format!("{}{}", keysastransit.to_str().unwrap(), "/");
    write_and_hash_file(file.to_str().unwrap(), "file.txt", &keysastransit).unwrap();
    assert_eq!(true, Path::new(&outfile).exists());
    drop(dir);
}
#[test]
fn test_get_extension_from_file() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    let _output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(get_extension_from_file(file.to_str().unwrap()), Some("txt"));
    let file = dir.path().join("file");
    let _output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(get_extension_from_file(file.to_str().unwrap()), None);
    drop(dir);
}
#[test]
fn test_get_metadata() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    let _output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(get_metadata(file.to_str().unwrap()).is_ok(), true);
    drop(dir);
}
#[test]
fn test_yara_scan() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let filerule = dir.path().join("index.yar");

    let rule = "rule contains_rust {
                    strings:
                        $rust = \"rust\" nocase
                    condition:
                        $rust
                }";
    fs::write(&filerule, rule).expect("Unable to write fake index rule");
    assert_eq!(true, Path::new(&filerule).exists());
    let filescan = dir.path().join("yara_scan.txt");

    fs::write(&filescan, "hello rust !").expect("Unable to write yara_scan.txt for testing");
    let mut matched_rules = Vec::new();
    matched_rules.push(String::from("contains_rust"));
    assert_eq!(
        yara_scan(&filescan.to_str().unwrap(), filerule.to_str().unwrap(), 10),
        Some(matched_rules)
    );
    fs::remove_file(&filescan).unwrap();
    assert_ne!(true, Path::new(&filescan).exists());
    fs::write(&filescan, "hello ruby !").expect("Unable to write yara_scan.txt for testing");
    assert_eq!(
        yara_scan(filescan.to_str().unwrap(), filerule.to_str().unwrap(), 10),
        None
    );
    drop(dir);
}

#[test]
fn test_yara_try() {
    let rule = "rule contains_rust {
                    strings:
                        $rust = \"rust\" nocase
                      condition:
                        $rust
                }";
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let filerule = dir.path().join("try.yar");
    fs::write(&filerule, rule).expect("Unable to write fake try rule");
    assert_eq!(true, Path::new(&filerule).exists());
    let filescan = dir.path().join("scanme.txt");
    let filescan_no_ext = dir.path().join("scanme");
    fs::write(&filescan, "hello rust !").unwrap();
    fs::write(&filescan_no_ext, "hello rust !").unwrap();
    yara_try(
        filescan.to_str().unwrap(),
        filerule.to_str().unwrap(),
        10,
        10000000,
        false,
    )
    .unwrap();
    // Scan a file without "extension"
    yara_try(
        filescan_no_ext.to_str().unwrap(),
        filerule.to_str().unwrap(),
        10,
        10000000,
        false,
    )
    .unwrap();
    let report = dir.path().join("scanme.txt.yara");
    let report_no_ext = dir.path().join("scanme.yara");
    assert_eq!(true, Path::new(&report).exists());
    assert_eq!(true, Path::new(&report_no_ext).exists());
    assert_eq!(true, Path::new(&filescan).exists());
    assert_eq!(true, Path::new(&filescan_no_ext).exists());

    // Cleaning the reports now and start again with yara_clean = true
    fs::remove_file(report).unwrap();
    fs::remove_file(report_no_ext).unwrap();
    yara_try(
        filescan.to_str().unwrap(),
        filerule.to_str().unwrap(),
        10,
        10000000,
        true,
    )
    .unwrap();
    // Scan a file without "extension"
    yara_try(
        filescan_no_ext.to_str().unwrap(),
        filerule.to_str().unwrap(),
        10,
        10000000,
        true,
    )
    .unwrap();
    let report = dir.path().join("scanme.txt.yara");
    let report_no_ext = dir.path().join("scanme.yara");
    assert_eq!(true, Path::new(&report).exists());
    assert_eq!(true, Path::new(&report_no_ext).exists());
    assert_eq!(false, Path::new(&filescan).exists());
    assert_eq!(false, Path::new(&filescan_no_ext).exists());

    // Now file is bigger that maxfilesize with
    // yara_clean = true
    fs::write(&filescan, "Hello, world!").unwrap();
    yara_try(
        filescan.to_str().unwrap(),
        filerule.to_str().unwrap(),
        10,
        0,
        true,
    )
    .unwrap();
    assert_eq!(false, Path::new(&filescan).exists());
    // yara_clean = false
    fs::write(&filescan, "Hello, world!").unwrap();
    yara_try(
        filescan.to_str().unwrap(),
        filerule.to_str().unwrap(),
        10,
        0,
        false,
    )
    .unwrap();
    assert_eq!(true, Path::new(&filescan).exists());
    drop(dir);
}
#[test]
fn test_clean_sas_dir() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("keysas");
    let _output = fs::create_dir_all(&path).unwrap();
    let file = path.join("file.txt");
    let file2 = path.join("file2.txt");
    let dir = path.join("directory");
    let symlink = path.join("symlink");
    let _output = File::create(&file).unwrap();
    let _output = File::create(&file2).unwrap();
    let _output = fs::create_dir(&dir).unwrap();
    let _output = std::os::unix::fs::symlink(&file, &symlink).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&dir).exists());
    assert_eq!(true, Path::new(&symlink).exists());
    let path = format!("{}{}", path.to_str().unwrap(), "/");
    clean_sas_dir(&path).unwrap();
    assert_eq!(false, Path::new(&file).exists());
    assert_eq!(false, Path::new(&file2).exists());
    assert_eq!(false, Path::new(&dir).exists());
    assert_eq!(false, Path::new(&symlink).exists());
    drop(dir)
}
#[test]
fn test_read_files() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("diodeout");
    let _output = fs::create_dir_all(&path).unwrap();
    let file = path.join("file.txt");
    let file2 = path.join("file2.txt");
    let file3 = path.join("file3.txt");
    let _output = File::create(&file).unwrap();
    let _output = File::create(&file2).unwrap();
    let _output = File::create(&file3).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&file2).exists());

    let path_keysasout = dir.path().join("keysasout");
    let _output = fs::create_dir_all(&path_keysasout).unwrap();

    let diode = format!("{}{}", &path.to_str().unwrap(), "/");
    let path_keysasout_string = format!("{}{}", &path_keysasout.to_str().unwrap(), "/");

    read_files(&path_keysasout_string, &diode).unwrap();
    let file = path_keysasout.join("file.txt");
    let file2 = path_keysasout.join("file2.txt");
    let file3 = path_keysasout.join("file3.txt");
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&file3).exists());
    drop(dir);
}
#[test]
fn test_remove_forbidden_extensions() {
    use std::io::prelude::*;
    use tempfile::tempdir;
    use zip::write::FileOptions;

    let dir = tempdir().unwrap();
    let path = dir.path().join("forbidden_files");
    let _output = fs::create_dir_all(&path).unwrap();
    let file = path.join("file.txt");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    let toobig = path.join("report.toobig");
    File::create(&toobig).unwrap();
    assert_eq!(true, toobig.exists());
    let av = path.join("report.antivirus");
    File::create(&av).unwrap();
    assert_eq!(true, av.exists());
    let yara = path.join("report.yara");
    File::create(&yara).unwrap();
    assert_eq!(true, yara.exists());
    let failed = path.join("report.failed");
    File::create(&failed).unwrap();
    assert_eq!(true, failed.exists());
    let path_to_compress = path.join("compress");
    let _output = fs::create_dir_all(&path_to_compress).unwrap();
    let allowed = vec![""];
    let _ret = remove_forbidden_extensions(file.to_str().unwrap(), allowed);
    assert_eq!(false, file.exists());
    // Now testing a forbidden zip
    let file = path.join("test.zip");
    let file_created = std::fs::File::create(&file).unwrap();

    let mut zip = zip::ZipWriter::new(file_created);
    zip.add_directory("test/", Default::default()).unwrap();
    let options = FileOptions::default()
        .compression_method(zip::CompressionMethod::Stored)
        .unix_permissions(0o755);
    zip.start_file("test/☃.txt", options).unwrap();
    zip.write_all(b"Hello, World!\n").unwrap();
    zip.finish().unwrap();
    let allowed = vec!["zip"];
    let _ret = remove_forbidden_extensions(file.to_str().unwrap(), allowed);
    assert_eq!(true, file.exists());
    let allowed = vec![""];
    let _ret = remove_forbidden_extensions(file.to_str().unwrap(), allowed);
    assert_eq!(false, file.exists());
    assert_eq!(true, av.exists());
    assert_eq!(true, yara.exists());
    assert_eq!(true, toobig.exists());
    assert_eq!(true, failed.exists());
    let report = format!("{}{}", file.to_string_lossy(), ".forbidden");
    println!("{}", report);
    assert_eq!(true, Path::new(&report).exists());

    drop(dir);
}
#[test]
fn test_write_fifo() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let filein = dir.path().join("file.in");
    let fileout = dir.path().join("file.out");
    File::create(&filein).unwrap();
    fs::write(&filein, "test").unwrap();
    File::create(&fileout).unwrap();
    assert_eq!(true, filein.exists());
    assert_eq!(true, fileout.exists());
    let lock = Arc::new(Mutex::new(true));
    write_fifo(
        fileout.to_str().unwrap(),
        "/file.in",
        dir.path().to_str().unwrap(),
        &lock,
    )
    .unwrap();
    let mut input = File::open(filein).unwrap();
    let mut input2 = File::open(fileout).unwrap();
    let mut sha256_1: Sha256 = Sha256::new();
    let mut sha256_2: Sha256 = Sha256::new();
    std::io::copy(&mut input, &mut sha256_1).unwrap();
    std::io::copy(&mut input2, &mut sha256_2).unwrap();
    let digest1 = sha256_1.finalize();
    let digest2 = sha256_2.finalize();
    assert_eq!(digest1, digest2);
}
