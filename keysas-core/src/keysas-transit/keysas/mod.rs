// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-transit".
 *
 * (C) Copyright 2019-2023 Stephane Neveu
 *
 * This file contains various functions
 * for building the keysas-in binary.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

use crate::errors::*;
use nix::sys::stat;
use nix::unistd;
use regex::Regex;
use scoped_thread_pool::Pool;
use sha2::{Digest, Sha256};
use std::cmp::Ordering;
use std::fs;
use std::fs::metadata;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;
use std::os::linux::fs::MetadataExt;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};
use syscallz::{Context as cont, Syscall};
use yara::*;
mod test_mod;

/// list_files(directory: &str)
/// This function list every files found in a directory.
pub fn list_files(directory: &str) -> Result<Vec<String>> {
    let paths: std::fs::ReadDir = fs::read_dir(directory)?;
    let names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();
    Ok(names)
}

/// get_extension_from_file(filename: &str)
/// This function gets the extension of a file and returns
/// an optional &str.
pub fn get_extension_from_file(filename: &str) -> Option<&str> {
    debug!("Getting extension for : {}", filename);
    Path::new(filename).extension().and_then(|s| s.to_str())
}

/// read_files(keysasout: &str, diode: &str)
/// This function reads from the named pipes "diode" and writes the data
/// to the filesystem in "keysasout"
/// This function discards hidden files.
/// This function log the files that are passed to keysasout, expect the utility files (.sha, .failed...)
/// This function is multithreaded.
pub fn read_files(keysasout: &str, diode: &str) -> Result<()> {
    let paths: std::fs::ReadDir = fs::read_dir(diode)?;
    let mut names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();
    //Not returning any files starting with dot like .bashrc
    let re = Regex::new(r"^\.([a-z])*")?;
    names.retain(|x| !re.is_match(x));

    let pool = Pool::new(num_cpus::get());

    pool.scoped(|scope| {
        for name in names {
            scope.execute(move || {
                let path_to_file = format!("{}{}", diode, name.as_str());
                if !Path::new(&path_to_file).is_dir() {
                    match write_and_hash_file(path_to_file.as_str(), name.as_str(), keysasout) {
                        Ok(()) => debug!("write_and_hash ok for file {}.", name),
                        Err(e) => error!("Error writing file to fs: {:?}", e),
                    }
                }
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// get_metadata(filename: &str)
/// This function prints metadata from file path
/// into the default logger.
pub fn get_metadata(filename: &str) -> Result<()> {
    if let Ok(metadata) = metadata(filename) {
        info!(
            "File owner: {} is uid {} with gid: {}.",
            filename,
            metadata.st_uid(),
            metadata.st_gid()
        );
    }
    Ok(())
}
/// clean_keysas_dir(path: &str)
/// This function removes every files or directories left while
/// starting up.
/// This function is multithreaded.
/// path must be a valid a writable directory (&str)
pub fn clean_sas_dir(path: &str) -> Result<()> {
    println!("Entering {} to clean files", &path);
    let names = list_files(path)?;
    let pool = Pool::new(num_cpus::get());
    pool.scoped(|scope| {
        for name in names {
            scope.execute(move || {
                let remove_me = format!("{}{}", path, name);
                if let Ok(n) = metadata(remove_me.as_str()) {
                    if n.is_dir() {
                        remove(&remove_me);
                        warn!("Directory {} removed when starting up.", remove_me);
                    } else if n.is_file() {
                        remove(&remove_me);
                        warn!("File {} removed when starting up.", remove_me);
                    } else {
                        remove(&remove_me);
                        warn!("Diode {} removed when starting up.", remove_me);
                    }
                } else {
                    remove(&remove_me);
                    error!("Cannot retrieve metadata on file: {}.", remove_me);
                }
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// yara_scan(path: &str, rules_path: &str, yara_timeout: u16)
/// This function scans a file with liyara using a defined ruleset
/// and returns a vector of Strings containing the rules matching.
fn yara_scan(path: &str, rules_path: &str, yara_timeout: u16) -> Option<Vec<String>> {
    info!("{} is scanning: {}", "Yara:", path);
    if Path::new(path).exists() {
        let compiler = Compiler::new().ok()?;
        debug!("{}", rules_path);
        match compiler.add_rules_file_with_namespace(rules_path, "keysas") {
            Ok(compiler) => {
                debug!("Yara ruleset {} added to namespace.", rules_path);
                match compiler.compile_rules() {
                    Ok(rules) => {
                        match rules.scan_file(path, yara_timeout.into()) {
                            Ok(results) => {
                                if !results.is_empty() {
                                    //Collect identifiers into a Vec of strings
                                    let rules: Vec<String> =
                                        results.iter().map(|n| n.identifier.to_owned()).collect();
                                    Some(rules)
                                } else {
                                    None
                                }
                            }
                            Err(e) => {
                                remove(path);
                                error!(
                                    "Yara cannot scan path {}: {:?}. Removing the file... ",
                                    path, e
                                );
                                None
                            }
                        }
                    }
                    Err(why) => {
                        error!("Yara cannot compile the ruleset: {:?}", why);
                        None
                    }
                }
            }
            Err(e) => {
                error!(
                    "Yara compiler cannot add ruleset to namespace: {:?}, removing file {}.",
                    e, path
                );
                remove(path);
                None
            }
        }
    } else {
        error!("{} File doesn't exists, cannot scan: {}", "Yara:", &path);
        None
    }
}

/// yara_try(path: &str, rules_path: &str, yara_timeout: u16, yara_maxfilesize: u64, yara_clean: bool)
/// This function checks the maximum file size to be scanned by libyara,
/// prints into default logger and create a report if libyara has matched with one or more rule.
/// We do not scan any *.sha256 files previously created by keysas-in.
/// If yara_clean is set, files bigger than the yara_maxfilesize or matching a rule are removed.
pub fn yara_try(
    path: &str,
    rules_path: &str,
    yara_timeout: u16,
    yara_maxfilesize: u64,
    yara_clean: bool,
) -> Result<()> {
    get_metadata(path)?;

    let file: u64 = metadata(path)?.len();
    if Path::new(rules_path).exists() {
        match file.cmp(&yara_maxfilesize) {
            Ordering::Less | Ordering::Equal => {
                match get_extension_from_file(path) {
                    None => match yara_scan(path, rules_path, yara_timeout) {
                        None => {
                            info!("{}Path {} seems clean.", "Yara: ", path);
                        }
                        Some(rules) => {
                            warn!(
                                "{} matched at least {} rule on path {}.",
                                "Yara:", rules[0], path
                            );
                            let path_to_yara_report = format!("{}{}", path, ".yara");
                            match fs::write(path_to_yara_report, rules.join("\n")) {
                                Ok(()) => debug!("Report successfully created."),
                                Err(e) => error!("Error while writing create report: {:?}", e),
                            }
                            if yara_clean {
                                remove(path);
                            }
                        }
                    },
                    Some(ext) => {
                        match ext {
                            //Do not catch *.sha256, *.toobig etc files for yara_scan
                            "sha256" => (),
                            "toobig" => (),
                            "antivirus" => (),
                            "failed" => (),
                            _ => match yara_scan(path, rules_path, yara_timeout) {
                                None => {
                                    info!("{}Path {} seems clean.", "Yara: ", path);
                                }
                                Some(rules) => {
                                    warn!(
                                        "{} matched at least {} rule on path {}.",
                                        "Yara:", rules[0], path
                                    );
                                    let path_to_yara_report = format!("{}{}", path, ".yara");
                                    match fs::write(path_to_yara_report, rules.join("\n")) {
                                        Ok(()) => debug!("Report successfully created."),
                                        Err(e) => {
                                            error!("Error while writing create report: {:?}", e)
                                        }
                                    }
                                    if yara_clean {
                                        remove(path);
                                    }
                                }
                            },
                        }
                    }
                }
            }
            Ordering::Greater => {
                if yara_clean {
                    remove(path);
                    warn!(
                        "{}Removing, file {} is bigger than YARA_MAXFILESIZE.",
                        "Yara: ", path
                    );
                } else {
                    warn!(
                        "{}Skipping, file {} is bigger than YARA_MAXFILESIZE.",
                        "Yara: ", path
                    );
                }
            }
        }
    } else {
        error!(
            "{}Cannot scan: Path to rules index.yar {} does not exist !",
            "Yara: ", rules_path
        );
    }
    Ok(())
}

/// mkfifo(diode_out: &str)
/// This funtion creates a read only named pipe for lekeysas-out
/// as the daemon is member of the group.
/// Each name pipe is created with the name of the incoming file
/// so each file has his own named pipe as it was previously
/// time-stamped by lekeysas-in.
fn mkfifo(diode_out: &str) {
    let fifo_path = PathBuf::from(diode_out);
    if Path::new(diode_out).exists() {
        warn!("Cleaning the previous diode !!");
        remove(diode_out);
    }

    // This creates a named pipe acting like a one-way diode.
    match unistd::mkfifo(&fifo_path, stat::Mode::S_IRGRP | stat::Mode::S_IWUSR) {
        Ok(_) => debug!("Software diode successfully created {:?}.", fifo_path),
        Err(err) => error!("Error while creating the software diode: {}.", err),
    }
}
/// write_fifo(diode_out: &str, file_name: &str, keysas_transit: &str)
/// This function reads the data from disk and writes it into the dedicated
/// software diode.
fn write_fifo(
    diode_out: &str,
    file_name: &str,
    keysas_transit: &str,
    lock: &Arc<Mutex<bool>>,
) -> Result<()> {
    debug!("Writing file into the diode !");
    debug!("diode_out path is: {}", diode_out);
    let path_to_file = format!("{}{}", keysas_transit, file_name);
    debug!("Write to file : {}", path_to_file);

    // Open the source file for reading
    let src_fd = nix::fcntl::open(
        Path::new(&path_to_file),
        nix::fcntl::OFlag::O_RDONLY,
        stat::Mode::empty(),
    )?;

    let data = lock.lock().unwrap();
    #[cfg(target_os = "linux")]
    init_seccomp_thread_write()
        .expect("Cannot apply seccomp syscall filter into thread for writing to named pipe.");
    drop(data);

    // Open the destination file for writing
    let dst_fd = nix::fcntl::open(diode_out, nix::fcntl::OFlag::O_WRONLY, stat::Mode::empty())?;

    // Copy the contents of the source file to the dedicated named pipe
    let mut buf = [0; 4096];
    loop {
        let n = nix::unistd::read(src_fd, &mut buf)?;
        if n == 0 {
            break;
        }
        nix::unistd::write(dst_fd, &buf[..n])?;
    }
    nix::unistd::close(dst_fd)?;
    Ok(())
}

/// This function reads the content of the file at "path_to_file" and copies it in keysas_transit
/// Also if it logs its hash
fn write_and_hash_file(path_to_file: &str, name: &str, keysas_transit: &str) -> Result<()> {
    let path_to_write = format!("{}{}", keysas_transit, name);
    if Path::new(path_to_file).exists() {
        let in_file = File::options()
            .read(true)
            .write(false)
            .open(path_to_file)
            .context("Cannot open input file")?;
        let mut out_file = File::create(path_to_write).context("Cannot create output file")?;
        let mut sha256: Sha256 = Sha256::new();
        let mut reader = BufReader::with_capacity(1048576, in_file);

        loop {
            let length = {
                let buffer = reader.fill_buf()?;
                out_file.write_all(buffer)?;
                sha256.update(buffer);
                buffer.len()
            };
            if length == 0 {
                break;
            }
            reader.consume(length);
        }
        let hash = sha256.finalize();
        info!("File {} sha256 hash is: {:x}", name, hash);
    } else {
        warn!("write_file: path to diode doesn't exists anymore !");
    }
    Ok(())
}

/// remove(file: &str)
/// This function removes a file, a named pipe or a directory.
/// We try to avoid crashing the daemon if permissions are wrong and it is not
/// possible to remove the file. Under normal circumstances, permissions are good
/// but we cannot avoid the root user to create unregular files.
fn remove(file: &str) {
    if Path::new(file).exists() {
        if Path::new(file).is_file() {
            if fs::remove_file(file).is_ok() {
                debug!("Removing file : {}", file);
            } else {
                error!("remove fn: Cannot remove file: {} !", file);
            }
        } else if Path::new(file).is_dir() {
            if fs::remove_dir_all(file).is_ok() {
                debug!("Removing unexpected directory: {}.", file);
            } else {
                error!("remove fn: Cannot remove unexpected directory: {}.", file);
            }
        } else if fs::remove_file(file).is_ok() {
            debug!("Removing named pipe: {}", file);
        } else {
            error!("remove fn: Cannot remove named pipe: {} !", file);
        }
    }
}

/// remove_forbidden_extensions(path: &str, forbidden_files: Vec<&str>)
/// This function removes forbidden files according to their magic number
/// using the infer crate.
fn remove_forbidden_extensions(path: &str, magiclist: Vec<&str>) {
    match infer::get_from_path(path) {
        Ok(Some(info)) => {
            debug!(
                "Mime type for file {} is: {} and extension is {} 🙂",
                path,
                info.mime_type(),
                info.extension()
            );
            if !magiclist.contains(&info.extension()) {
                remove(path);
                warn!(
                    "File {}, magic number {} not allowed ! 😨 Removing...",
                    path,
                    info.mime_type()
                );
                let path_forbidden = format!("{}{}", path, ".forbidden");
                match fs::write(
                    path_forbidden,
                    "File type is not allowed by your administrator.",
                ) {
                    Ok(()) => debug!("Report for forbidden file successfully created."),
                    Err(e) => error!("Error while writing report for forbiden file: {:?}", e),
                }
            }
        }
        Ok(None) => {
            match get_extension_from_file(path) {
                None => {
                    warn!("Unknown file type for {} 😞", path);
                    remove(path);
                    let path_forbidden = format!("{}{}", path, ".forbidden");
                    match fs::write(
                        path_forbidden,
                        "File type is not allowed by your administrator.",
                    ) {
                        Ok(()) => debug!("Report for forbidden file successfully created."),
                        Err(e) => error!("Error while writing report for forbiden file: {:?}", e),
                    }
                }
                Some(ext) => {
                    match ext {
                        // Do not catch *.sha256 *.yara etc files.
                        // As they are not real extensions, I cannot use infer
                        "sha256" => (),
                        "yara" => (),
                        "toobig" => (),
                        "antivirus" => (),
                        "failed" => (),
                        "forbidden" => (),
                        _ => {
                            warn!("Unallowed file type for {} 😞, removing...", path);
                            remove(path);
                            let path_forbidden = format!("{}{}", path, ".forbidden");
                            match fs::write(
                                path_forbidden,
                                "File type is not allowed by your administrator.",
                            ) {
                                Ok(()) => debug!("Report for forbidden file successfully created."),
                                Err(e) => {
                                    error!("Error while writing report for forbiden file: {:?}", e)
                                }
                            }
                        }
                    }
                }
            }
        }

        Err(e) => {
            error!("Looks like something went wrong 😨 : {:?}", e);
            remove(path);
        }
    }
}

/// analyse_transit_files(keysas_transit: &str, _diode_out: &str, rules_path: &str, yara_timeout: u16, yara_maxfilesize: u64, yara_clean: bool)
/// Multithreaded function that spawn a thread on each file, calls remove_forbidden_extensions and
/// yara_try.
/// We do not scan the files we created (*.sha256 and *.yara).
/// Note that an untrusted user won't be able to deposit files ending with .sha256 and .yara
/// in lekeysas-in.
pub fn analyse_transit_files(
    keysas_transit: &str,
    rules_path: &str,
    yara_timeout: u16,
    yara_maxfilesize: u64,
    yara_clean: bool,
    magic_list: Vec<&str>,
) -> Result<()> {
    let names = list_files(keysas_transit)?;
    let pool = Pool::new(num_cpus::get());
    pool.scoped(|scope| {
        for name in names {
            let magiclist = magic_list.clone();
            //Spawn a thread per file and do the job
            scope.execute(move || {
                let path_to_file = format!("{}{}", keysas_transit, name.as_str());
                if Path::new(&path_to_file).is_file() {
                    remove_forbidden_extensions(&path_to_file, magiclist);
                    if Path::new(&path_to_file).exists() {
                        match get_extension_from_file(&path_to_file) {
                            None => {
                                debug!("{}", "No extension received from get_extension_from_file.");
                                match yara_try(
                                    &path_to_file,
                                    rules_path,
                                    yara_timeout,
                                    yara_maxfilesize,
                                    yara_clean,
                                ) {
                                    Ok(()) => {
                                        debug!(
                                            "Yara_try successfully check {}.",
                                            path_to_file.as_str()
                                        )
                                    }
                                    Err(e) => error!(
                                        "Yara_try had an issue checking file {}: {:?}",
                                        path_to_file.as_str(),
                                        e
                                    ),
                                }
                            }
                            Some(ext) => {
                                match ext {
                                    // Do not catch *.sha256 and *.yara files for yara_scan
                                    // As sha256 and yara are not real extensions, I cannot use infer
                                    "sha256" => (),
                                    "yara" => (),
                                    "failed" => (),
                                    "toobig" => (),
                                    "antivirus" => (),
                                    "forbidden" => (),
                                    // Scanning with libyara is the default case
                                    _ => {
                                        if Path::new(&path_to_file).exists() {
                                            match yara_try(
                                                &path_to_file,
                                                rules_path,
                                                yara_timeout,
                                                yara_maxfilesize,
                                                yara_clean,
                                            ) {
                                                Ok(()) => debug!(
                                                    "Yara_try successfully check {}.",
                                                    path_to_file.as_str()
                                                ),
                                                Err(e) => error!(
                                                    "Yara_try had an issue checking file {}: {:?}",
                                                    path_to_file.as_str(),
                                                    e
                                                ),
                                            }
                                        } else {
                                            debug!(
                                                "Unallowed extension file {} already removed !",
                                                path_to_file
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    debug!(
                        "Unexpected file or directory {} found.",
                        path_to_file.as_str()
                    );
                    remove(path_to_file.as_str());
                } //endif
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// send_transit_files(keysas_transit: &str, diode_out: &str)
/// This multithreaded function apply a new seccomp filter in each thread
/// reads the data on the disk and write it to the dedicated software diode.
/// When data is read from the other side, we do some housekeeping removing
/// the named pipes and the data on disk.
#[cfg(not(tarpaulin_include))]
pub fn send_transit_files(keysas_transit: &str, diode_out: &str) -> Result<()> {
    let names = list_files(keysas_transit)?;
    let seccomp_lock = Arc::new(Mutex::new(true));
    let pool = Pool::new(num_cpus::get());
    pool.scoped(|scope| {
        for name in names {
            let path_to_file = format!("{}{}", keysas_transit, name.as_str());
            let path_to_fifo = format!("{}{}", diode_out, name.as_str());
            let lock = Arc::clone(&seccomp_lock);
            //Spawn a thread per file and do the job
            scope.execute(move || {
                if Path::new(&path_to_file).exists() && Path::new(&path_to_file).is_file() {
                    info!("Sending file: {}", name);
                    mkfifo(path_to_fifo.as_str());
                    debug!("Diode {} successfully created.", path_to_fifo.as_str());
                    #[cfg(target_os = "linux")]
                    match write_fifo(path_to_fifo.as_str(), &name, keysas_transit, &lock) {
                        Ok(()) => debug!("write_fifo ok for file {}", name),
                        Err(e) => error!(
                            "write_fifo: Error writing file {}: {:?}",
                            path_to_fifo.as_str(),
                            e
                        ),
                    }
                    remove(path_to_fifo.as_str());
                    debug!("Diode {} successfully removed", path_to_fifo.as_str());
                    remove(path_to_file.as_str());
                    debug!("File {} successfully removed", path_to_file.as_str());
                }
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// init_seccomp_thread_write()
/// This function applies a lowered whitelist of syscalls
/// while starting writing in a thread.
#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init_seccomp_thread_write() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::write)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::exit_group)?;
    Ok(())
}
