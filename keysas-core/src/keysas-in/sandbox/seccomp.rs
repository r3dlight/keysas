// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-in".
 *
 * (C) Copyright 2019-2023 Stephane Neveu
 *
 * This file contains various funtions
 * to sandbox this binary using seccomp.
 */

use crate::errors::Result;
#[cfg(target_os = "linux")]
use syscallz::{Context, Syscall};

#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init() -> Result<()> {
    let mut ctx = Context::init()?;

    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::write)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::clock_nanosleep)?;
    ctx.allow_syscall(Syscall::sched_yield)?;
    ctx.allow_syscall(Syscall::seccomp)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::unlink)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::futex)?;
    ctx.allow_syscall(Syscall::munmap)?;
    #[cfg(not(target_arch = "arm"))]
    ctx.allow_syscall(Syscall::mmap)?;
    #[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64")))]
    ctx.allow_syscall(Syscall::mmap2)?;
    ctx.allow_syscall(Syscall::fstat)?;
    ctx.allow_syscall(Syscall::newfstatat)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::mknod)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::mknodat)?;
    ctx.allow_syscall(Syscall::mprotect)?;
    ctx.allow_syscall(Syscall::sched_getaffinity)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::stat)?;
    ctx.allow_syscall(Syscall::statx)?;
    ctx.allow_syscall(Syscall::getcwd)?;
    ctx.allow_syscall(Syscall::getsockopt)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::connect)?;
    ctx.allow_syscall(Syscall::recvfrom)?;
    ctx.allow_syscall(Syscall::madvise)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::sendto)?;
    ctx.allow_syscall(Syscall::socket)?;
    ctx.allow_syscall(Syscall::nanosleep)?;
    ctx.allow_syscall(Syscall::getdents64)?;
    ctx.allow_syscall(Syscall::set_robust_list)?;
    #[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64")))]
    ctx.allow_syscall(Syscall::lstat)?;
    ctx.allow_syscall(Syscall::clone)?;
    ctx.allow_syscall(Syscall::fcntl)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rename)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::renameat)?;
    ctx.allow_syscall(Syscall::execve)?;
    ctx.allow_syscall(Syscall::brk)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rmdir)?;
    ctx.allow_syscall(Syscall::ioctl)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::mkdir)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::mkdirat)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::poll)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::ppoll)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::access)?;
    ctx.allow_syscall(Syscall::faccessat)?;
    ctx.allow_syscall(Syscall::rt_sigaction)?;
    ctx.allow_syscall(Syscall::lseek)?;
    ctx.allow_syscall(Syscall::getrandom)?;
    ctx.allow_syscall(Syscall::rt_sigprocmask)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::arch_prctl)?;
    ctx.allow_syscall(Syscall::prctl)?;
    ctx.allow_syscall(Syscall::set_tid_address)?;
    ctx.allow_syscall(Syscall::exit)?;
    ctx.allow_syscall(Syscall::exit_group)?;
    ctx.allow_syscall(Syscall::prlimit64)?;
    ctx.allow_syscall(Syscall::mremap)?;
    ctx.allow_syscall(Syscall::openat)?;
    ctx.allow_syscall(Syscall::clock_gettime)?;
    ctx.allow_syscall(Syscall::ioprio_get)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::readlink)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::readlinkat)?;
    ctx.load()?;
    Ok(())
}
