// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-in".
 *
 * (C) Copyright 2019-2023 Stephane Neveu, Luc Bonnafoux
 *
 * This file contains the main function.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

#[macro_use]
extern crate log;
use crate::errors::*;
use clap::{arg, crate_version, Command};
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use landlock::{
    path_beneath_rules, Access, AccessFs, Ruleset, RulesetAttr, RulesetCreatedAttr, RulesetError,
    RulesetStatus, ABI,
};
use std::net::IpAddr;
use std::path::Path;
use std::process;
use std::thread as main_thread;
use std::time::Duration;

mod errors;
mod keysas;
mod sandbox;

fn landlock_sandbox() -> Result<(), RulesetError> {
    let abi = ABI::V1;
    let status = Ruleset::new()
        .handle_access(AccessFs::from_all(abi))?
        .create()?
        // Read-only access.
        .add_rules(path_beneath_rules(
            &["/etc/keysas"],
            AccessFs::from_read(abi),
        ))?
        // Read-write access.
        .add_rules(path_beneath_rules(
            &["/run/diode-in", "/var/local/in", "/var/log/keysas-in"],
            AccessFs::from_all(abi),
        ))?
        .restrict_self()?;
    match status.ruleset {
        // The FullyEnforced case must be tested.
        RulesetStatus::FullyEnforced => println!("Landlock: Fully sandboxed."),
        RulesetStatus::PartiallyEnforced => println!("Landlock: Partially sandboxed."),
        // Users should be warned that they are not protected.
        RulesetStatus::NotEnforced => {
            println!("Landlock: Not sandboxed! Please update your kernel.")
        }
    }
    Ok(())
}

// Defines configuration and main loop.
#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    // Start clap CLI definition

    let matches = Command::new("keysas-in")
        .version(crate_version!())
        .author("Stephane N.")
        .about("keysas-in, input window.")
        .arg(
            arg!( -g --sasin <PATH> "Sets Keysas's path for incoming files")
                .default_value("/var/local/in/"),
        )
        .arg(
            arg!( -d --diodein <PATH> "Sets the diode-in folder path for transfering files")
                .default_value("/run/diode-in/"),
        )
        .arg(
            arg!( -l --log <PATH> "Sets a custom log file path")
                .default_value("/var/log/keysas-in/"),
        )
        .arg(arg!( -c --clamavip <IP> "Clamav IP addr").default_value("127.0.0.1"))
        .arg(
            arg!( -p --clamavport <PORT> "Clamav port number")
                .default_value("3310")
                .value_parser(clap::value_parser!(u16)),
        )
        .arg(
            arg!( -s --rotate_over_size <SIZE> "Rotate logs over specified size (bytes)")
                .default_value("3000000")
                .value_parser(clap::value_parser!(u64)),
        )
        .arg(
            arg!( -n --number_of_log_files <NUMBER> "Number of log files to keep")
                .default_value("3")
                .value_parser(clap::value_parser!(usize)),
        )
        .arg(
            arg!( -m --maxfilesize <NUMBER> "Maximum file size (in bytes) allowed for transfering")
                .default_value("500000000")
                .value_parser(clap::value_parser!(u64)),
        )
        .get_matches();

    // Get values or use default ones.
    let keysasin = matches.get_one::<String>("sasin").unwrap();
    let diode = matches.get_one::<String>("diodein").unwrap();
    let log_path = matches.get_one::<String>("log").unwrap();
    let clamav_ip = matches.get_one::<String>("clamavip").unwrap();
    let clamav_port = matches.get_one::<u16>("clamavport").unwrap();
    //let clamav_port_int = clamav_port
    //    .parse::<u16>()
    //    .context("Cannot convert clamav port string into u16 integer !")?;
    let rotate_over_size = matches.get_one::<u64>("rotate_over_size").unwrap();
    let nb_of_log_files = matches.get_one::<usize>("number_of_log_files").unwrap();
    let maxfilesize = matches.get_one::<u64>("maxfilesize").unwrap();

    //let rotate_over_size = rotate_over_size
    //    .parse::<u64>()
    //    .context("Cannot convert rotate_over_size value string into u64 integer !")?;
    //let nb_of_log_files = nb_of_log_files
    //    .parse::<usize>()
    //    .context("Cannot convert nb_of_log_files value string into usize integer !")?;
    //let maxfilesize = maxfilesize
    //    .parse::<u64>()
    //    .context("Cannot convert maxfilesize value string into u64 integer !")?;

    // Check that log_path is valid.
    if !Path::new(log_path).exists()
        || !Path::new(log_path).is_dir()
        || Path::new(log_path).metadata()?.permissions().readonly()
    {
        panic!("Log path {} is not valid ! Please check if the directory exists and check the permissions.", log_path)
    }

    // Logger init and parameters.
    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Warn)
        .rotate(
            Criterion::Size(*rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(*nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // Setting up Lanlock sandbox.
    #[cfg(target_os = "linux")]
    match landlock_sandbox() {
        Ok(_) => (),
        Err(_e) => {
            error!("Failed to start landlock sandbox");
            process::exit(1);
        }
    }
    // Setting up allowed syscalls.
    #[cfg(target_os = "linux")]
    match sandbox::init() {
        Ok(_o) => (),
        Err(_e) => {
            error!("Cannot apply first seccomp mode 2 syscall filter.");
            process::exit(1);
        }
    }

    // Check that diode in and keysasin exist before going futher.
    if !Path::new(diode).exists()
        || !Path::new(diode).is_dir()
        || Path::new(diode).metadata()?.permissions().readonly()
    {
        error!(
            "Path to diode-in {} not valid ! This should normaly be created by systemd.",
            diode
        );
        process::exit(1);
    }
    if !Path::new(keysasin).exists()
        || !Path::new(keysasin).is_dir()
        || Path::new(keysasin).metadata()?.permissions().readonly()
    {
        error!("Path to keysasin {} is not valid ! Please check if the directory exists and check the permissions.", keysasin);
        process::exit(1);
    }
    // Check that the Clamav IP is valid.
    match clamav_ip.parse::<IpAddr>() {
        Ok(_) => (),
        Err(e) => {
            error!("The IP provided for Clamav is not valid ! {:?}", e);
            process::exit(1);
        }
    }

    // Logger is set, so I can use it.
    info!("{}", "Keysas-In is starting");
    info!("Incoming sas : {}", keysasin);
    info!("In-Out diode folder path : {}", diode);
    info!("Logfile : {}", log_path);
    info!("Clamav IP : {}", clamav_ip);
    info!("Clamav port: {}", clamav_port);
    info!("Rotate logs over size : {:?}", rotate_over_size);
    info!("Number of logs to keep : {:?}", nb_of_log_files);

    // Housekeeping while starting up: removing everything or don't start
    match keysas::clean_sas_dir(keysasin) {
        Ok(_o) => (),
        Err(_e) => {
            error!("Error in path KEYSASIN: Directory is not empty and cannot be cleaned, please check permissions.");
            process::exit(1);
        }
    }
    match keysas::clean_sas_dir(diode) {
        Ok(_o) => (),
        Err(_e) => {
            error!("Error in path DIODE: Directory is not empty and cannot be cleaned, please check permissions.");
            process::exit(1);
        }
    }

    // Check if clamd is responding
    if keysas::clamd_version(clamav_ip, *clamav_port).is_err() {
        error!("Failed to start clamd");
        process::exit(1);
    }

    // First scan begins here
    match keysas::clamd_scan(keysasin, clamav_ip, *clamav_port) {
        Ok(_o) => (),
        Err(_e) => {
            error!("Failed to perform clamd scan");
            process::exit(1);
        }
    }

    info!("Watching {} directory for activity...", keysasin);

    // Starting the main loop.
    let lock = format!("{}{}", keysasin, ".lock");

    loop {
        if !Path::new(&lock).exists() {
            // Sanitize all forbidden files like dirs or .{yara|sha256}
            keysas::sanitize_all(keysasin).unwrap_or_else(|e| {
                if e == KeysasErrorCode::UnrecoverableErr {
                    error!("Failed to sanitize files");
                    process::exit(1);
                }
            });
            //Hash the content
            keysas::hash_folder_content(keysasin)?;
            // AV scanning
            keysas::clamd_scan(keysasin, clamav_ip, *clamav_port).unwrap_or_else(|e| {
                if e == KeysasErrorCode::UnrecoverableErr {
                    error!("Failed to scan files");
                    process::exit(1);
                }
            });
            // Remove files larger than maxfilesize and unwanted
            // extensions.
            keysas::remove_toobig(keysasin, *maxfilesize).unwrap_or_else(|e| {
                if e == KeysasErrorCode::UnrecoverableErr {
                    error!("Failed to remove files too big");
                    process::exit(1);
                }
            });
            // Finally, send those files.
            keysas::send_clean_files(keysasin, diode).unwrap_or_else(|e| {
                if e == KeysasErrorCode::UnrecoverableErr {
                    error!("Failed to send clean files");
                    process::exit(1);
                }
            });
            main_thread::sleep(Duration::from_millis(500));
        }
    }
}
