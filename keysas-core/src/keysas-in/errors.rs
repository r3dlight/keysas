// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "Keysas-in".
 *
 * (C) Copyright 2019-2023 Stephane Neveu
 *
 * This file use failure for error handling.
 */
pub use anyhow::{Context, Result};

#[derive(Debug, PartialEq)]
pub enum KeysasErrorCode {
    UnrecoverableErr,
    RecoverableErr,
}
