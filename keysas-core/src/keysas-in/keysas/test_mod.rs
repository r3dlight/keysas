// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-in".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the tests.
 */

#[cfg(test)]
use super::*;

#[test]
fn test_sha256_digest() {
    use std::io::Write;
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("sha256.txt");

    let mut output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(file.to_str().unwrap()).exists());
    write!(output, "We will generate a sha256 hash of this text").unwrap();
    drop(output);
    //let input = File::open(file).unwrap();
    //let reader = BufReader::new(input);
    let digest = sha256_digest(&file.to_str().unwrap()).unwrap();
    let good_hash: &str = "c64bd6723c08b96eb68841f7fe790acd078d65c87f1b8876e378d410f61e1ea0";
    //let good_hash = vec![
    //    198, 75, 214, 114, 60, 8, 185, 110, 182, 136, 65, 247, 254, 121, 10, 205, 7, 141, 101, 200,
    //    127, 27, 136, 118, 227, 120, 212, 16, 246, 30, 30, 160,
    //];

    assert_eq!(
        //HEXLOWER.encode(digest.as_ref()),
        digest,
        good_hash
    );
    //println!("{:?}",digest.to_vec());
    drop(dir)
}
#[test]
fn test_create_sha256() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    File::create(&file).unwrap();
    create_sha256(&file).unwrap();
    let sha256 = dir.path().join("file.txt.sha256");
    assert_eq!(true, Path::new(sha256.to_str().unwrap()).exists());
    drop(dir)
}

#[test]
fn test_remove() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    let _output = File::create(&file).unwrap();
    remove(file.to_str().unwrap()).unwrap();
    assert_eq!(false, Path::new(&file).exists());
}
#[test]
fn test_remove_dir() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("directory");
    let _output = fs::create_dir_all(&path).unwrap();
    assert_eq!(true, Path::new(&path).exists());
    remove_dir(path.to_str().unwrap()).unwrap();
    assert_eq!(false, Path::new(&path).exists());
    drop(dir);
}
#[test]
fn test_mkfifo() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("test_fifo");
    mkfifo(&path.to_str().unwrap()).unwrap();
    assert_eq!(true, Path::new(&path).exists());
    remove(&path.to_str().unwrap()).unwrap();
    assert_ne!(true, Path::new(&path).exists());
    drop(dir)
}
#[test]
fn test_clean_sas_dir() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("keysas");
    let _output = fs::create_dir_all(&path).unwrap();
    let file = path.join("file.txt");
    let file2 = path.join("file2.txt");
    let dir = path.join("directory");
    let symlink = path.join("symlink");
    let _output = File::create(&file).unwrap();
    let _output = File::create(&file2).unwrap();
    let _output = fs::create_dir(&dir).unwrap();
    let _output = std::os::unix::fs::symlink(&file, &symlink).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&dir).exists());
    assert_eq!(true, Path::new(&symlink).exists());
    let path = format!("{}{}", path.to_str().unwrap(), "/");
    clean_sas_dir(&path).unwrap();
    assert_eq!(false, Path::new(&file).exists());
    assert_eq!(false, Path::new(&file2).exists());
    assert_eq!(false, Path::new(&dir).exists());
    assert_eq!(false, Path::new(&symlink).exists());
    drop(dir)
}

#[test]
fn test_list_files() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("transit");
    let _output = fs::create_dir_all(&path).unwrap();
    assert_eq!(true, Path::new(&path).exists());
    let file = path.join("file.txt");
    let _output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    let files = list_files(path.to_str().unwrap());
    assert_eq!(files.unwrap(), ["file.txt"]);
    drop(dir);
}

#[test]
fn test_hash_folder_content() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    File::create(file).unwrap();
    let tmpdir = format!("{}{}", &dir.path().to_str().unwrap(), "/");
    hash_folder_content(&tmpdir).unwrap();
    let shafile = dir.path().join("file.txt.sha256");
    assert_eq!(true, Path::new(&shafile).exists());
    drop(dir);
}
#[test]
fn test_is_not_hidden() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join(".file");
    File::create(&file).unwrap();
    assert!(Path::new(&file).is_file());
    let file2 = dir.path().join("file");
    File::create(&file2).unwrap();
    assert!(Path::new(&file2).is_file());
    let mut count: i32 = 0;
    for entry in WalkDir::new(dir.path().to_str().unwrap())
        .into_iter()
        .filter_entry(|e| is_not_hidden(e))
        .filter_map(|v| v.ok())
    {
        match count {
            0 => {
                assert_eq!(
                    format!("{}{}", "/tmp/", entry.file_name().to_string_lossy()),
                    dir.path().to_str().unwrap()
                );
                count = count + 1;
            }
            1 => assert_eq!(entry.file_name().to_string_lossy(), "file"),
            _ => (),
        }
    }
    drop(dir);
}
#[test]
fn test_clamd_scan() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file");
    File::create(&file).unwrap();
    let virus = dir.path().join("virus");
    File::create(&virus).unwrap();
    assert_eq!(true, Path::new(&file).is_file());
    fs::write(
        &virus,
        "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*",
    )
    .unwrap();
    assert_eq!(true, Path::new(&virus).is_file());
    let tmpdir = format!("{}{}", dir.path().to_str().unwrap(), "/");
    clamd_scan(&tmpdir, "127.0.0.1", 3310).unwrap();
    assert_eq!(false, Path::new(&virus).exists());
    let report = format!("{}{}", virus.to_string_lossy(), ".antivirus");
    assert_eq!(true, Path::new(&report).exists());
    assert_eq!(true, Path::new(&report).is_file());
    assert_eq!(true, Path::new(&file).exists());
    drop(dir)
}
#[test]
fn test_check_size() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file");
    File::create(&file).unwrap();
    check_size(&file, 10000).unwrap();
    assert_eq!(true, file.exists());
    fs::write(&file, "Feeding the cat to make it bigger").unwrap();
    check_size(&file, 0).unwrap();
    assert_eq!(false, file.exists());
    let report = format!("{}{}", file.to_string_lossy(), ".toobig");
    assert_eq!(true, Path::new(&report).exists());
    assert_eq!(true, Path::new(&report).is_file());
}
#[test]
fn test_remove_toobig() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file");
    File::create(&file).unwrap();
    remove_toobig(dir.path().to_str().unwrap(), 10000).unwrap();
    assert_eq!(true, file.exists());
}
#[test]
fn test_sanitize() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.sha256");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    sanitize(&file, dir.path().to_str().unwrap()).unwrap();
    assert_eq!(false, file.exists());
    let file = dir.path().join("file.yara");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    sanitize(&file, dir.path().to_str().unwrap()).unwrap();
    assert_eq!(false, file.exists());
    let file = dir.path().join("file.toobig");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    sanitize(&file, dir.path().to_str().unwrap()).unwrap();
    assert_eq!(false, file.exists());
    let file = dir.path().join("file.antivirus");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    sanitize(&file, dir.path().to_str().unwrap()).unwrap();
    assert_eq!(false, file.exists());
    let p = dir.into_path();
    sanitize(&p, p.to_str().unwrap()).unwrap();
    assert_eq!(true, p.try_exists().unwrap());
}
#[test]
fn test_get_metadata() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.txt");
    let _output = File::create(&file).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(get_metadata(file.to_str().unwrap()).is_ok(), true);
    drop(dir);
}
#[test]
fn test_clamd_version() {
    clamd_version("127.0.0.1", 3310).unwrap();
}
#[test]
fn test_sanitize_all() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file = dir.path().join("file.sha256");
    File::create(&file).unwrap();
    assert_eq!(true, file.exists());
    let file2 = dir.path().join("file.yara");
    File::create(&file2).unwrap();
    assert_eq!(true, file2.exists());
    sanitize_all(dir.path().to_str().unwrap()).unwrap();
    assert_eq!(false, file.exists());
    assert_eq!(false, file2.exists());
}
#[test]
fn test_write_fifo() {
    use tempfile::tempdir;
    let dir = tempdir().unwrap();
    let filein = dir.path().join("file.in");
    let fileout = dir.path().join("file.out");
    File::create(&filein).unwrap();
    fs::write(&filein, "test").unwrap();
    File::create(&fileout).unwrap();
    assert_eq!(true, filein.exists());
    assert_eq!(true, fileout.exists());
    let lock = Arc::new(Mutex::new(true));
    write_fifo(
        fileout.to_str().unwrap(),
        "/file.in",
        dir.path().to_str().unwrap(),
        &lock,
    )
    .unwrap();
    let digest = sha256_digest(filein.to_str().unwrap()).unwrap();
    let digest2 = sha256_digest(fileout.to_str().unwrap()).unwrap();
    assert_eq!(digest, digest2,);
}
