// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "keysas-in".
 *
 * (C) Copyright 2019-2023 Stephane Neveu, Luc Bonnafoux
 *
 * This file contains various funtions
 * for building the keysas-in binary.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

use crate::errors::*;
use clam_client::client::ClamClient;
use clam_client::response::ClamScanResult;
use nix::sys::stat;
use nix::unistd;
use regex::Regex;
use scoped_thread_pool::Pool;
use sha2::{Digest, Sha256};
use std::cmp::Ordering;
use std::fs;
use std::fs::File;
use std::fs::{metadata, OpenOptions};
use std::io::BufReader;
use std::io::{Read, Write};
use std::os::linux::fs::MetadataExt;
use std::path::{Path, PathBuf};
use std::process::exit;
use std::sync::{Arc, Mutex};
use syscallz::{Context as cont, Syscall};
use time::OffsetDateTime;
use walkdir::{DirEntry, WalkDir};
mod test_mod;

/// Call sha256_digest and write a .sha256 file containing the digest of the original file
///
/// # Example
///
/// ```
/// use tempfile::tempdir;
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// File::create(&file).unwrap();
/// create_sha256(&file).unwrap();
/// let sha256 = dir.path().join("file.txt.sha256");
/// assert_eq!(true, Path::new(sha256.to_str().unwrap()).exists());
/// ```
pub fn create_sha256(file: &Path) -> Result<()> {
    if file.exists() {
        let f: std::fs::Metadata = metadata(file)?;
        if f.is_file() {
            info!("Creating sha256 digest for {}", &file.to_string_lossy());
            let digest = sha256_digest(&file.to_string_lossy())?;
            let mut path = PathBuf::new();
            path.push(file);
            match path.to_str() {
                Some(p) => {
                    let path_to_sha = format!("{}{}", p, ".sha256");
                    let _output = File::create(&path_to_sha)?;
                    let file = OpenOptions::new().write(true).open(&path_to_sha)?;
                    writeln!(&file, "{}", digest)?;
                    info!("File: {}, digest: {}", path_to_sha, digest);
                }
                //Should not happen
                None => error!("Cannot convert path to str to create .sha256 file"),
            }
        } else if f.is_dir() {
            info!("Directory, skipped");
        } else {
            error!(
                "Path {:?} is not a file. Cannot create sha256 digest.",
                file
            );
        }
    } else {
        error!("File {:?} not found. Cannot create sha256 digest.", file);
    }
    Ok(())
}

/// Creates a sha256 hash from a file
///
/// # Example
///
/// ```
/// use tempfile::tempdir;
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let digest = sha256_digest(&file.to_str().unwrap()).unwrap();
/// let good_hash: &str = "c64bd6723c08b96eb68841f7fe790acd078d65c87f1b8876e378d410f61e1ea0";
/// assert_eq!(digest, good_hash);
/// ```
pub fn sha256_digest(file: &str) -> Result<String> {
    let input = File::open(file)?;
    let mut reader = BufReader::new(input);

    let digest = {
        let mut hasher = Sha256::new();
        let mut buffer = [0; 1048576];
        loop {
            let count = reader.read(&mut buffer)?;
            if count == 0 {
                break;
            }
            hasher.update(&buffer[..count]);
        }
        hasher.finalize()
    };
    Ok(format!("{:x}", digest))
}

/// Gets metadata from a file and log it into default logger
///
/// # Example
///
/// ```
/// use tempfile::tempdir;
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// assert_eq!(get_metadata(file.to_str().unwrap()).is_ok(), true);
/// ```
pub fn get_metadata(filename: &str) -> Result<()> {
    if let Ok(metadata) = metadata(filename) {
        info!(
            "File owner for {} is uid {} with gid: {}.",
            filename,
            metadata.st_uid(),
            metadata.st_gid()
        );
    }
    Ok(())
}

fn write_toobig_report(path: &String, file_size: u64) -> Result<()> {
    let _output = File::create(path)?;
    let file = OpenOptions::new().write(true).open(path)?;
    writeln!(
        &file,
        "File is bigger than the maximum allowed by your administrator: {} bytes. Removing...",
        file_size
    )?;
    Ok(())
}

/// Check the file size and remove it if the size is above MAXFILESIZE
///
/// # Example
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let result = check_size(file, 0);
/// assert_eq!(false, file.exists());
/// ```
fn check_size(f: &Path, maxfilesize: u64) -> Result<(), KeysasErrorCode> {
    let file = match f.to_str() {
        Some(s) => s,
        None => {
            warn!("Invalid file name");
            return Err(KeysasErrorCode::RecoverableErr);
        }
    };
    let mdata = match metadata(file) {
        Ok(o) => o,
        Err(_e) => {
            error!("Invalid file metadata");
            return Err(KeysasErrorCode::RecoverableErr);
        }
    };
    let file_size: u64 = mdata.len();
    match file_size.cmp(&maxfilesize) {
        Ordering::Less | Ordering::Equal => {}
        Ordering::Greater => {
            warn!("File {} is larger than MAXFILESIZE, removing it.", file);
            match remove(file) {
                Ok(_o) => {
                    let path_to_size = format!("{}{}", file, ".toobig");
                    match write_toobig_report(&path_to_size, file_size) {
                        Ok(_o) => (),
                        Err(_e) => {
                            error!("Failed to write toobig report");
                            return Err(KeysasErrorCode::RecoverableErr);
                        }
                    }
                }
                Err(_e) => {
                    error!("Failed to remove file toot big: {}", file);
                    return Err(KeysasErrorCode::UnrecoverableErr);
                }
            }
        }
    }
    Ok(())
}

/// This function removes the file if:
///     - its extension is .yara, .sha256, .antivirus or .toobig.
///     OR
///     - it starts with dot
///
/// # Example
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let hash = dir.path().join("file.sha256");
/// let _output = File::create(&file).unwrap();
/// let _output = File::create(&hash).unwrap();
/// let result = sanitize(file);
/// assert_eq!(true, file.exists());
/// let result = sanitize(hash);
/// assert_eq!(false, hash.exists());
/// ```
fn sanitize(f: &Path, keysasin: &str) -> Result<(), KeysasErrorCode> {
    let file = match f.to_str() {
        Some(s) => s,
        None => {
            warn!("Invalid file name");
            return Err(KeysasErrorCode::RecoverableErr);
        }
    };
    let re = match Regex::new(r"^\.([a-z])*") {
        Ok(r) => r,
        Err(_e) => {
            warn!("Failed to construct regex");
            return Err(KeysasErrorCode::RecoverableErr);
        }
    };
    if let Ok(n) = metadata(file) {
        if n.is_dir() {
            if Path::new(file) != Path::new(keysasin) {
                match remove_dir(file) {
                    Ok(()) => warn!("Directory {} removed when starting up.", file),
                    Err(e) => {
                        error!("Cannot remove folder in keysas in after starting : {:?}", e);
                        return Err(KeysasErrorCode::UnrecoverableErr);
                    }
                }
            }
        } else if n.is_file() {
            if re.is_match(file) {
                match remove(file) {
                    Ok(_o) => (),
                    Err(_e) => {
                        error!("Failed to remove file {}", file);
                        return Err(KeysasErrorCode::UnrecoverableErr);
                    }
                }
            }
            if let Some(ext) = get_extension_from_file(file) {
                match ext {
                    "sha256" | "yara" | "antivirus" | "toobig" | "forbidden" => {
                        warn!("Removing unallowed file with {} extention: {}.", ext, file);
                        match remove(file) {
                            Ok(_o) => (),
                            Err(_e) => {
                                error!("Failed to remove file {}", file);
                                return Err(KeysasErrorCode::UnrecoverableErr);
                            }
                        }
                    }
                    _ => (),
                }
            }
        } else {
        }
    } else {
        warn!("Cannot get metadata for file: {}", file);
    }
    Ok(())
}

/// This function removes every files or directories left while
/// starting up.
/// This function is multithreaded.
/// The path must be a valid a writable directory (&str)
///
/// # Example
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let hash = dir.path().join("file.sha256");
/// let _output = File::create(&file).unwrap();
/// let _output = File::create(&hash).unwrap();
/// let result = clean_keysas_dir(dir.path().to_str().unwrap());
/// assert_eq!(false, file.exists());
/// assert_eq!(false, hash.exists());
/// ```
pub fn clean_sas_dir(path: &str) -> Result<(), KeysasErrorCode> {
    info!("Entering {} to clean files", &path);
    let names = list_files(path)?;
    let pool = Pool::new(num_cpus::get());
    pool.scoped(|scope| {
        for name in names {
            scope.execute(move || {
                let remove_me = format!("{}{}", path, name);
                if let Ok(n) = metadata(remove_me.as_str()) {
                    if n.is_dir() {
                        match remove_dir(&remove_me) {
                            Ok(()) => {
                                warn!("Directory {} removed when starting up.", remove_me)
                            }
                            Err(e) => {
                                error!("Cannot remove folder in keysas in after starting : {:?}", e)
                            }
                        }
                    } else if n.is_file() {
                        match remove(&remove_me) {
                            Ok(()) => warn!("File {} removed when starting up.", remove_me),
                            Err(e) => {
                                error!("Cannot remove file in keysas in after starting : {:?}", e)
                            }
                        }
                        warn!("File removed while starting {}", &remove_me)
                    } else {
                        match remove(&remove_me) {
                            Ok(()) => warn!("Diode {} removed when starting up.", remove_me),
                            Err(e) => error!("Cannot remove named pipes after starting : {:?}", e),
                        }
                    }
                } else {
                    error!("Cannot get metadata for file : {}", remove_me);
                    match remove(&remove_me) {
                        Ok(()) => warn!("File {} removed when starting up.", remove_me),
                        Err(e) => {
                            error!("Cannot remove file {} after starting : {:?}", remove_me, e)
                        }
                    }
                }
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// This function prints clamd version or panic.
/// Clamav is mandatory to start the daemon.
///
/// Example:
///
/// ```
/// assert_eq!(true, clamd_version("127.0.0.1", 3310).is_ok());
/// ```
pub fn clamd_version(clamav_ip: &str, clamav_port_int: u16) -> Result<()> {
    if let Ok(client) = ClamClient::new_with_timeout(clamav_ip, clamav_port_int, 5) {
        info!("{:?}", client.version().expect("Clamav server is not responding ! Please fix this issue, killing the daemon for now..."));
        println!("{:?}", client.version().expect("Clamav server is not responding ! Please fix this issue, killing the daemon for now..."));
    } else {
        error!("Clamav server is not responding ! Please fix this issue, killing the daemon for now...");
        println!("Clamav server is not responding ! Please fix this issue, killing the daemon for now....");
        exit(1);
    }
    Ok(())
}

/// get_extension_from_file(filename: &str)
/// This function gets the (fake) extension of a file and returns
/// an optional &str.
///
/// Example:
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let extension = get_extension_from_file(file.to_str().unwrap());
/// assert_eq("txt", extension);
/// ```
pub fn get_extension_from_file(filename: &str) -> Option<&str> {
    debug!("Getting extension for : {}", filename);
    Path::new(filename).extension().and_then(|s| s.to_str())
}

fn clamd_create_report(path: &String, virus: &String) -> Result<()> {
    let _output = File::create(path)?;
    let file = OpenOptions::new().write(true).open(path)?;
    writeln!(&file, "Malicious file detected: {}", virus)?;
    Ok(())
}

/// This function scans files in a directory using Clamd.
/// You might want to set a clamd IP & port according to your needs.
///
/// Example:
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let _result = clamd_scan(dir.path().to_str().unwrap(),"127.0.0.1", 3310)
/// ```
pub fn clamd_scan(
    keysasin: &str,
    clamav_ip: &str,
    clamav_port_int: u16,
) -> Result<(), KeysasErrorCode> {
    let client = match ClamClient::new(clamav_ip, clamav_port_int) {
        Ok(o) => o,
        Err(_e) => {
            error!("Cannot create a new instance of ClamClient.");
            return Err(KeysasErrorCode::UnrecoverableErr);
        }
    };
    if let Ok(results) = client.scan_path(keysasin, true) {
        for scan_result in results.iter() {
            if let ClamScanResult::Found(location, virus) = scan_result {
                info!("Clamav: Found virus: '{}' in {}", virus, location);
                warn!("{} Found virus: '{}' in {}", "Clamav:", virus, location);
                match remove(location) {
                    Ok(_o) => (),
                    Err(_e) => {
                        error!("Failed to remove virus at {}", location);
                        return Err(KeysasErrorCode::UnrecoverableErr);
                    }
                }
                let path_to_report = format!("{}{}", location, ".antivirus");
                match clamd_create_report(&path_to_report, virus) {
                    Ok(_o) => (),
                    Err(_e) => {
                        error!("Failed to create virus report for {}", virus);
                        // Continue to do the scan
                    }
                }
            }
        }
    }
    Ok(())
}

/// This function returns a bool
/// True if file does not end with filepart or start with . and depth=0
/// else false
/// This is a filter on regular files ans not hidden ones.
/// We do not want to catch .bashrc like files.
///
/// Example:
/// ```
/// let walker = WalkDir::new("foo").into_iter();
/// for entry in walker.filter_entry(|e| !is_not_hidden(e)) {
///    let entry = entry.unwrap();
///    println!("{}", entry.path().display());
/// ```
fn is_not_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| entry.depth() == 0 || !s.starts_with("."))
        .unwrap_or(false)
}

/// This function recursively parse all files in a folder
/// and creates sha256 digest for each file.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// File::create(file).unwrap();
/// let tmpdir = format!("{}{}", &dir.path().to_str().unwrap(), "/");
/// hash_folder_content(&tmpdir).unwrap();
/// let shafile = dir.path().join("file.txt.sha256");
/// assert_eq!(true, Path::new(&shafile).exists());
/// ```
pub fn hash_folder_content(keysasin: &str) -> Result<()> {
    WalkDir::new(keysasin)
        .into_iter()
        .filter_entry(is_not_hidden)
        .filter_map(|v| v.ok())
        .try_for_each(|x| create_sha256(x.path()))
}

/// This function ecursively parse all files in a folder
/// to remove files larger than MAXFILESIZE value.
/// The functions stops only on catastrophic error, otherwise
/// go to next file.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file");
/// File::create(&file).unwrap();
/// remove_toobig(dir.path().to_str().unwrap(), 10000).unwrap();
/// assert_eq!(true, file.exists());
/// ```
pub fn remove_toobig(keysasin: &str, maxfilesize: u64) -> Result<(), KeysasErrorCode> {
    WalkDir::new(keysasin)
        .into_iter()
        .filter_entry(is_not_hidden)
        .filter_map(|v| v.ok())
        .try_for_each(|x| match check_size(&x.into_path(), maxfilesize) {
            Ok(_o) => Ok(()),
            Err(e) => match e {
                KeysasErrorCode::RecoverableErr => Ok(()),
                KeysasErrorCode::UnrecoverableErr => Err(e),
            },
        })
}

/// This function recursively parse all files in a folder
/// and call sanitize on each file to remove files with
/// extensions matching .sha256 or .yara.
/// The functions stops only on catastrophic error, otherwise
/// go to next file.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.sha256");
/// File::create(&file).unwrap();
/// assert_eq!(true, file.exists());
/// let file2 = dir.path().join("file.yara");
/// File::create(&file2).unwrap();
/// assert_eq!(true, file2.exists());
/// sanitize_all(dir.path().to_str().unwrap()).unwrap();
/// assert_eq!(false, file.exists());
/// assert_eq!(false, file2.exists());
/// ```
pub fn sanitize_all(keysasin: &str) -> Result<(), KeysasErrorCode> {
    WalkDir::new(keysasin)
        .into_iter()
        .filter_entry(is_not_hidden)
        .filter_map(|v| v.ok())
        .try_for_each(|x| match sanitize(&x.into_path(), keysasin) {
            Ok(_o) => Ok(()),
            Err(e) => match e {
                KeysasErrorCode::RecoverableErr => Ok(()),
                KeysasErrorCode::UnrecoverableErr => Err(e),
            },
        })
}

/// List clean files, creates a pipe for each one and call write_fifo
#[cfg(not(tarpaulin_include))]
pub fn send_clean_files(keysasin: &str, diode: &str) -> Result<(), KeysasErrorCode> {
    let names = list_files(keysasin)?;
    let pool = Pool::new(num_cpus::get());
    // Create a mutex for the seccomp configuration
    let seccomp_lock = Arc::new(Mutex::new(true));
    pool.scoped(|scope| {
        for name in names {
            let lock = Arc::clone(&seccomp_lock);
            let path_to_file = format!("{}{}", keysasin, name.as_str());
            let timestamp = format!(
                "{}-{}-{}_{}-{}-{}-{}",
                OffsetDateTime::now_utc().day(),
                OffsetDateTime::now_utc().month(),
                OffsetDateTime::now_utc().year(),
                OffsetDateTime::now_utc().hour(),
                OffsetDateTime::now_utc().minute(),
                OffsetDateTime::now_utc().second(),
                OffsetDateTime::now_utc().nanosecond()
            );
            let timestamp = str::replace(&timestamp, " ", "-");
            let path_to_fifo = format!("{}{}-{}", diode, timestamp, name.as_str());
            let n: std::fs::Metadata = match metadata(path_to_file.as_str()) {
                Ok(o) => o,
                Err(_e) => {
                    error!("Cannot get metadata in keysasin");
                    return;
                }
            };
            debug!("Got metadata for file: {:?}", &path_to_file);
            scope.execute(move || {
                if n.is_dir() {
                    match remove_dir(path_to_file.as_str()) {
                        Ok(()) => {
                            debug!("Successfully removed directory: {}.", path_to_file.as_str())
                        }
                        Err(e) => error!("Cannot remove directory: {:?}", e),
                    }
                } else if n.is_file() {
                    match get_metadata(&path_to_file) {
                        Ok(()) => debug!("Got metadata for {}.", path_to_file.as_str()),
                        Err(e) => error!("Problem while getting metadata: {:?}", e),
                    }
                    info!("Sending file: {}", name);

                    match mkfifo(path_to_fifo.as_str()) {
                        Ok(()) => debug!("Diode {} successfully created.", path_to_fifo.as_str()),
                        Err(e) => error!("Cannot create diode: {:?}", e),
                    }
                    match write_fifo(path_to_fifo.as_str(), &name, keysasin, &lock) {
                        Ok(()) => {
                            debug!("Successfully written into diode {}.", path_to_fifo.as_str())
                        }
                        Err(e) => error!("Error while creating the diode: {:?}", e),
                    }
                    match remove(path_to_fifo.as_str()) {
                        Ok(()) => debug!("Diode {} successfully removed", path_to_fifo.as_str()),
                        Err(e) => error!("Error while removing diode: {:?}", e),
                    }
                    match remove(path_to_file.as_str()) {
                        Ok(()) => debug!("File {} successfully removed", path_to_file.as_str()),
                        Err(e) => error!("Cannot remove file: {:?}", e),
                    }
                } else {
                    error!("Unknow object type");
                }
            });
        }
    });
    pool.shutdown();
    Ok(())
}

/// This function creates a named pipe.
/// Each name pipe is created with the name of the incoming file.
/// So each file has his own named pipe.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let path = dir.path().join("test_fifo");
/// mkfifo(&path.to_str().unwrap()).unwrap();
/// assert_eq!(true, Path::new(&path).exists());
/// remove(&path.to_str().unwrap()).unwrap();
/// assert_ne!(true, Path::new(&path).exists());
/// drop(dir)
/// ```
fn mkfifo(diode: &str) -> Result<()> {
    let fifo_path = PathBuf::from(diode);
    if Path::new(diode).exists() {
        warn!("Cleaning the previous diode !");
        remove(diode)?;
    }

    // Create a named pipe acting like a diode
    match unistd::mkfifo(&fifo_path, stat::Mode::S_IRGRP | stat::Mode::S_IWUSR) {
        Ok(_) => debug!("Name pipe created {:?}", fifo_path),
        Err(err) => error!("Error creating fifo: {}", err),
    }
    Ok(())
}

/// This function remove a file or a named pipe
/// Do not crash the daemon if permissions are
/// wrong and cannot remove the file !
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// remove(file.to_str().unwrap()).unwrap();
/// assert_eq!(false, Path::new(&file).exists());
/// ```
pub fn remove(file: &str) -> Result<()> {
    if Path::new(file).exists() {
        if fs::remove_file(file).is_ok() {
            debug!("Removing file : {}", file);
        } else {
            error!(
                "Cannot remove file: {} ! Please remove it manually if still present.",
                file
            );
        }
    }
    Ok(())
}

/// This function removes a directory.
/// We try to avoid crashing the daemon is permissions
/// are wrong and cannot remove the directory.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let path = dir.path().join("directory");
/// let _output = fs::create_dir_all(&path).unwrap();
/// assert_eq!(true, Path::new(&path).exists());
/// remove_dir(path.to_str().unwrap()).unwrap();
/// assert_eq!(false, Path::new(&path).exists());
/// ```
pub fn remove_dir(dir: &str) -> Result<()> {
    if Path::new(dir).is_dir() {
        if fs::remove_dir_all(dir).is_ok() {
            debug!("Removing directory : {}", dir);
        } else {
            error!(
                "Cannot remove directory: {} ! Please remove it manually if still present.",
                dir
            );
        }
    }
    Ok(())
}

/// This function lists all files in a directory except hidden ones.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let path = dir.path().join("transit");
/// let _output = fs::create_dir_all(&path).unwrap();
/// assert_eq!(true, Path::new(&path).exists());
/// let file = path.join("file.txt");
/// let _output = File::create(&file).unwrap();
/// assert_eq!(true, Path::new(&file).exists());
/// let files = list_files(path.to_str().unwrap());
/// assert_eq!(files.unwrap(), ["file.txt"]);
/// ```
pub fn list_files(directory: &str) -> Result<Vec<String>, KeysasErrorCode> {
    let paths: std::fs::ReadDir = match fs::read_dir(directory) {
        Ok(o) => o,
        Err(_e) => {
            warn!("Failed to open directory");
            return Err(KeysasErrorCode::RecoverableErr);
        }
    };
    let mut names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();
    // Not sending any files starting with dot like .bashrc nor *.filepart
    let re = match Regex::new(r#"^\.|\w+\.filepart$"#) {
        Ok(o) => o,
        Err(_e) => {
            warn!("Failed to create regex");
            return Err(KeysasErrorCode::RecoverableErr);
        }
    };
    names.retain(|x| !re.is_match(x));
    Ok(names)
}

/// This function writes files into a named pipe which is
/// a blocking operation until it is read by another
/// process for example.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let filein = dir.path().join("file.in");
/// let fileout = dir.path().join("file.out");
/// File::create(&filein).unwrap();
/// fs::write(&filein, "test").unwrap();
/// File::create(&fileout).unwrap();
/// assert_eq!(true, filein.exists());
/// assert_eq!(true, fileout.exists());
/// write_fifo(
///    fileout.to_str().unwrap(),
///    "/file.in",
///    dir.path().to_str().unwrap(),
///).unwrap();
/// let digest = sha256_digest(filein.to_str().unwrap()).unwrap();
/// let digest2 = sha256_digest(fileout.to_str().unwrap()).unwrap();
///assert_eq!(digest, digest2,);
/// ```
fn write_fifo(diode: &str, file_name: &str, keysasin: &str, lock: &Arc<Mutex<bool>>) -> Result<()> {
    debug!("Writing file {} into the diode !", file_name);
    debug!("Diode is {}", &diode);
    let path_to_file = format!("{}{}", keysasin, file_name);

    // Open the source file for reading
    let src_fd = nix::fcntl::open(
        Path::new(&path_to_file),
        nix::fcntl::OFlag::O_RDONLY,
        stat::Mode::empty(),
    )?;

    let data = lock.lock().unwrap();
    #[cfg(target_os = "linux")]
    init_seccomp_thread_write()
        .expect("Cannot apply seccomp syscall filter into thread for writing to named pipe.");
    drop(data);

    // Open the destination file for writing
    let dst_fd = nix::fcntl::open(diode, nix::fcntl::OFlag::O_WRONLY, stat::Mode::empty())?;

    // Copy the contents of the source file to the dedicated named pipe
    let mut buf = [0; 4096];
    loop {
        let n = nix::unistd::read(src_fd, &mut buf)?;
        if n == 0 {
            break;
        }
        nix::unistd::write(dst_fd, &buf[..n])?;
    }
    nix::unistd::close(dst_fd)?;
    Ok(())
}

#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
fn init_seccomp_thread_write() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::write)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::exit_group)?;
    Ok(())
}
