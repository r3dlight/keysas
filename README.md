[![Made with Rust](https://img.shields.io/badge/Made%20with-Rust-9cf)](https://www.rust-lang.org/)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![codecov](https://codecov.io/gl/r3dlight/keysas/branch/master/graph/badge.svg)](https://codecov.io/gl/r3dlight/keysas)

<div  align="center">

<img  src ="img/logo.svg"  alt="Keysas"  width=300px/>

</div>

  

# What is it ?

  
**Keysas** is a modern decontamination station prototype, fast, multithreaded which aims to be secure.

Keysas provides an easy solution to verify your documents. It can be used as a simple “decontamination station” or act as a gateway to transfert untrusted files to higher security level networks. 
You can manage and create your own Yara rules and add them easily to Keysas in order to inspect your files with more efficiency.

## User documentation

User documentation can be found here : [https://keysas.fr](https://keysas.fr)


## Gateway

<div  align="center">
<img  src ="img/flowchart.png"  alt="keysas gateway"  width=900px/>
</div>

## USB mode

<div  align="center">
<img  src ="img/usbmode.jpg"  alt="Keysas USB"  width=900px/>
</div>



## Security
  

- Memory-safe
- Thread-safe
- Mode 2 seccomp (x86_64 and aarch64 supported)
- Tested with cargo audit & Clippy
- Daemons are all running with unprivileged users
- Apparmor policies
- Landlock support
- Advanced Systemd protections per daemon tuned with **systemd-analyse security**:
<div  align="center">
<img  src ="img/systemd-security.png"  alt="systemd protections"  width=600px/>
</div>


**Note:** keysas is tested with Grsecurity kernel patch. It just requires MPROTECT to be disabled on ELF binaries.

# Installation

For the installation procedure, please follow the [online documentation](https://keysas.fr/installation.html).

## To do
  
- Debian packaging via Cargo
- Secomp strict 
